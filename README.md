# awesome-rust

A curated list of Rust code and resources.

## Table of contents

* [Applications](#applications)
  * [Audio and Music](#audio-and-music)
  * [Blockchain](#blockchain)
  * [Database](#database)
  * [Emulators](#emulators)
  * [File manager](#file-manager)
  * [Games](#games)
  * [Graphics](#graphics)
  * [Industrial automation](#industrial-automation)
  * [Observability](#observability)
  * [Operating systems](#operating-systems)
  * [Productivity](#productivity)
  * [Routing protocols](#routing-protocols)
  * [Security tools](#security-tools)
  * [Social networks](#social-networks)
  * [System tools](#system-tools)
  * [Text editors](#text-editors)
  * [Text processing](#text-processing)
  * [Utilities](#utilities)
  * [Video](#video)
  * [Virtualization](#virtualization)
  * [Window managers](#window-managers)
  * [Web](#web)
  * [Web Servers](#web-servers)
* [Development tools](#development-tools)
  * [Build system](#build-system)
  * [Debugging](#debugging)
  * [Deployment](#deployment)
  * [Embedded](#embedded)
  * [FFI](#ffi)
  * [Formatters](#formatters)
  * [IDEs](#ides)
  * [Pattern recognition](#pattern-recognition)
  * [Profiling](#profiling)
  * [Services](#services)
  * [Static analysis](#static-analysis)
  * [Testing](#testing)
  * [Transpiling](#transpiling)
* [Libraries](#libraries)
  * [Artificial Intelligence](#artificial-intelligence)
      * [Genetic algorithms](#genetic-algorithms)
      * [Machine learning](#machine-learning)
      * [OpenAI](#openai)
  * [Astronomy](#astronomy)
  * [Asynchronous](#asynchronous)
  * [Audio](#audio-1)
  * [Authentication](#authentication)
  * [Automotive](#automotive)
  * [Bioinformatics](#bioinformatics)
  * [Caching](#caching)
  * [Cloud](#cloud)
  * [Command-line](#command-line)
  * [Compression](#compression)
  * [Computation](#computation)
  * [Concurrency](#concurrency)
  * [Configuration](#configuration)
  * [Cryptography](#cryptography)
  * [Database](#database-1)
  * [Data streaming](#data-streaming)
  * [Data processing](#data-processing)
  * [Data structures](#data-structures)
  * [Data visualization](#data-visualization)
  * [Date and time](#date-and-time)
  * [Distributed systems](#distributed-systems)
  * [Domain driven design](#domain-driven-design)
  * [Email](#email)
  * [Encoding](#encoding)
  * [Filesystem](#filesystem)
  * [Financial](#financial)
      * [ISO8583](#ISO8583)
  * [Framework](#framework)
  * [Game development](#game-development)
  * [Geospatial](#geospatial)
  * [Graph algorithms](#graph-algorithms)
  * [Graphics](#graphics-1)
  * [Graph processing](#graph-processing)
  * [GUI](#gui)
  * [Image processing](#image-processing)
  * [Industrial automation](#industrial-automation)  
  * [Language specification](#language-specification)
  * [Logging](#logging)
  * [Macro](#macro)
  * [Markup language](#markup-language)
  * [Memory](#memory)
  * [Messaging](#messaging)
  * [Miscellaneous](#miscellaneous)
    * [Uncategorized](#uncategorized)
  * [Mobile](#mobile)
  * [Network programming](#network-programming)
  * [Parsing](#parsing)
  * [Peripherals](#peripherals)
  * [Platform specific](#platform-specific)
  * [QrCode](#qrcode)
  * [Scripting](#scripting)
  * [Simulation](#simulation)
  * [Task scheduling](#task-scheduling)
  * [Template engine](#template-engine)
  * [Text processing](#text-processing-1)
  * [Text search](#text-search)
  * [Video](#video-1)
  * [Virtualization](#virtualization-1)
  * [Web programming](#web-programming)
* [Registries](#registries)
* [Resources](#resources)

## Applications

See also [Rust - Production](https://www.rust-lang.org/production)(organizations running Rust in production).

* [A/B Street](https://github.com/a-b-street/abstreet) - Transportation planning and traffic simulation software for creating cities friendlier to walking, biking, and public transit.
* [alacritty](https://github.com/alacritty/alacritty) - A cross-platform, GPU enhanced terminal emulator
* [api](https://github.com/intecture/api) - an API-driven server management and configuration tool.
* [Arti](https://gitlab.torproject.org/tpo/core/arti) — An implementation of Tor, in Rust. (So far, it's a not-very-complete client. But watch this space!)
* [asm-cli-rust](https://github.com/cch123/asm-cli-rust) - interative assembly shell written in rust.
* [boringtun](https://github.com/cloudflare/boringtun) - A Userspace WireGuard VPN Implementation.
* [datafusion](https://github.com/apache/datafusion) - Apache Arrow DataFusion and Ballista query engines.
* [defguard](https://github.com/defguard/defguard) — Enterprise Open Source SSO & WireGuard VPN with real 2FA/MFA.
* [deno](https://github.com/denoland/deno) — A secure JavaScript/TypeScript runtime built with V8 and Tokio
* [device-blocker](https://github.com/darrint/device-blocker) - Limit screen time to children's various mobile devices by blocking internet access on the family Wifi router.
* [dipc](https://github.com/doprz/dipc) - Convert your favorite images and wallpapers with your favorite color palettes/themes.
* [diwata](https://github.com/ivanceras/diwata) - A database administration tool for postgresql.
* [docli](https://github.com/kbknapp/docli-rs) - A command line utility for managing DigitalOcean infrastructure.
* [doh-server](https://github.com/jedisct1/doh-server) - A fast and secure DoH (DNS-over-HTTPS) server written in Rust.
* [drill](https://github.com/fcsonline/drill) - A HTTP load testing application inspired by Ansible syntax.
* [EasyTier](https://github.com/KKRainbow/EasyTier) - A simple, full-featured and decentralized mesh VPN with WireGuard support.
* [eidolon](https://github.com/nicohman/eidolon) - A steam and drm-free game registry and launcher for linux and macosx.
* [Factotum](https://github.com/snowplow/factotum) - [A system to programmatically run data pipelines](https://snowplowanalytics.com/blog/2016/04/09/introducing-factotum-data-pipeline-runner/).
* [fend](https://github.com/printfn/fend) - Arbitrary-precision unit-aware calculator.
* [flowgger](https://github.com/jedisct1/flowgger) - A fast, simple and lightweight data collector.
* [Fractal](https://github.com/danigm/fractal) - Fractal is a Matrix messaging app for GNOME written in Rust. Its interface is optimized for collaboration in large groups, such as free software projects.
* [Fractalide](https://github.com/fractalide/fractalide) - Simple Rust Microservices
* [generic-dns-update](https://github.com/dlecan/generic-dns-update) - A tool to update DNS zonefiles with your IP address.
* [habitat](https://github.com/habitat-sh/habitat) - An tool created by [Chef](https://www.chef.io/) to build, deploy, and manage applications.
* [Herd](https://github.com/imjacobclark/Herd) - an experimental HTTP load testing application.
* [innernet](https://github.com/tonarino/innernet) - An overlay or private mesh network that uses Wireguard under the hood.
* [kalker](https://github.com/PaddiM8/kalker) - A scientific calculator that supports math-like syntax with user-defined variables, functions, derivation, integration, and complex numbers. Cross platform + WASM support.
* [Kubernetes Dashboard](https://github.com/kubernetes/dashboard) - General-purpose web UI for Kubernetes clusters.
* [kytan](https://github.com/changlan/kytan) - High Performance Peer-to-Peer VPN.
* [s3find-rs](https://github.com/AnderEnder/s3find-rs) - A command line utility to walk an Amazon S3 hierarchy, an analog of find for Amazon S3.
* [rust-genetic-algorithm](https://github.com/andschwa/rust-genetic-algorithm) - A genetic algorithm for academic benchmark problems.
* [linkerd2-proxy](https://github.com/linkerd/linkerd2-proxy) - Ultralight service mesh for Kubernetes.
* [limonite](https://crates.io/crates/limonite) - static blog/website generator.
* [MaidSafe](https://maidsafe.net) - A decentralized platform.
* [mdBook](https://crates.io/crates/mdbook) - A command line utility to create books from markdown files.
* [mirrord](https://github.com/metalbear-co/mirrord) - Connect your local process and your cloud environment, and run local code in cloud conditions.
* [notty](https://github.com/withoutboats/notty) - A new kind of terminal.
* [Pijul](https://pijul.org) - A patch-based distributed version control system.
* [Rio](https://github.com/raphamorim/rio) - A hardware-accelerated GPU terminal emulator powered by WebGPU, focusing to run in desktops and browsers.
* [rsign](https://crates.io/crates/rsign) - A simple command-line tool used to generate/sign/verify digital signatures designed to be compatible with Minisign.
* [rx](https://github.com/cloudhead/rx) — Vi inspired Modern Pixel Art Editor.
* [Sandstorm Collections App](https://github.com/sandstorm-io/collections-app)
* [Servo](https://github.com/servo/servo) - A prototype web browser engine.
* [shoes](https://github.com/cfal/shoes) - A multi-protocol proxy server.
* [shuttle](https://github.com/shuttle-hq/shuttle) - A serverless platform built for Rust.
* [Sniffnet](https://github.com/GyulyVGC/sniffnet) - Cross-platform application to monitor your network traffic with ease.
* [SWC](https://github.com/swc-project/swc) - super-fast TypeScript / JavaScript compiler.
* [trust-dns](https://crates.io/crates/trust-dns) - A DNS-server.
* [wasmer](https://github.com/wasmerio/wasmer) - A safe and fast WebAssembly runtime supporting WASI and Emscripten.
* [Weld](https://github.com/serayuzgur/weld) - Full fake REST API generator.
* [zellij](https://github.com/zellij-org/zellij) - A terminal multiplexer (workspace) with batteries included.

### Audio and Music

* [dano](https://github.com/kimono-koans/dano) - A hashdeep/md5tree (but much more) for media files.
* [enginesound](https://github.com/DasEtwas/enginesound) - A GUI and command line application used to procedurally generate semi-realistic engine sounds. Featuring in-depth configuration, variable sample rate and a frequency analysis window.
* [Festival](https://github.com/hinto-janai/festival) - A local music player/server/client.
* [Glicol](https://github.com/chaosprint/glicol) - Graph-oriented live coding language written in Rust for collaborative musicking in browsers.
* [Lyriek](https://github.com/bartwillems/lyriek) - A multi-threaded GTK 3 application for fetching the lyrics of the current playing song.
* [mmtc](https://github.com/figsoda/mmtc) - Minimal mpd terminal client that aims to be simple yet highly configurable.
* [Pinepods](https://github.com/madeofpendletonwool/PinePods) — A rust based podcast management system with multi-user support. Pinepods utilizes a central database so aspects like listen time and themes follow from device to device. With clients built using Tauri, it's a full cross-platform listening solution.
* [Polaris](https://github.com/agersant/polaris) - A music streaming application.
* [Spotify Player](https://github.com/aome510/spotify-player) - A Spotify player in the terminal with full feature parity.
* [Spotify TUI](https://github.com/Rigellute/spotify-tui) - A Spotify client for the terminal written in Rust.
* [Spotifyd](https://github.com/Spotifyd/spotifyd) - An open source Spotify client running as a UNIX daemon.
* [zrtstr](https://github.com/indiscipline/zrtstr) - A command line utility for checking if stereo wav files are faux-stereo (i.e. have identical channels) and converting such files to mono.

### Blockchain

* [Akula](https://github.com/akula-bft/akula) - Rust Ethereum Execution Layer (EL) Client (WIP).
* [artemis](https://github.com/paradigmxyz/artemis) - A simple, modular, and fast framework for writing MEV bots in Rust.
* [beerus](https://github.com/eigerco/beerus) - Beerus is a trustless StarkNet Light Client, ⚡blazing fast ⚡ and powered by Rust.
* [bip39-rs](https://github.com/infincia/bip39-rs) - Rust implementation of BIP39.
* [Bitcoin Satoshi's Vision](https://github.com/brentongunning/rust-sv) - A Rust library for working with Bitcoin SV.
* [cairo](https://github.com/starkware-libs/cairo) - Cairo is the first Turing-complete language for creating provable programs for general computation. This is also the native language of [StarkNet](https://starknet.io), a ZK-Rollup using STARK proofs
* [cardano-cli](https://github.com/input-output-hk/cardano-cli) - Cardano Command Line Interface (CLI).
* [ChainX](https://github.com/chainx-org/ChainX) - Fully Decentralized Interchain Crypto Asset Management on Polkadot.
* [CITA](https://github.com/citahub/cita) — A high performance blockchain kernel for enterprise users.
* [coinbase-pro-rs](https://github.com/inv2004/coinbase-pro-rs) — Coinbase pro client in Rust, supports sync/async/websocket.
* [Diem](https://github.com/diem/diem) - Diem’s mission is to enable a simple global currency and financial infrastructure that empowers billions of people.
* [electrumrs](https://github.com/romanz/electrs) - An efficient re-implementation of Electrum Server in Rust.
* [ethabi](https://github.com/rust-ethereum/ethabi) - Encode and decode smart contract invocations.
* [ethaddrgen](https://github.com/Limeth/ethaddrgen) - Custom Ethereum vanity address generator made in Rust.
* [ethers-rs](https://github.com/gakonst/ethers-rs) - Complete Ethereum & Celo library and wallet implementation in Rust.
* [etk](https://github.com/quilt/etk) - etk is a collection of tools for writing, reading, and analyzing EVM bytecode.
* [Forest](https://github.com/ChainSafe/forest) - Rust Filecoin implementation.
* [Foundry](https://github.com/foundry-rs/foundry) - Foundry is a blazing fast, portable and modular toolkit for Ethereum application development written in Rust.
* [Grin](https://github.com/mimblewimble/grin/) - Evolution of the MimbleWimble protocol
* [hdwallet](https://github.com/jjyr/hdwallet) - BIP-32 HD wallet related key derivation utilities.
* [Holochain](https://github.com/holochain/holochain) - Scalable P2P alternative to blockchain for all those distributed apps you always wanted to build.
* [ibc-rs](https://github.com/informalsystems/hermes) - Rust implementation of the [Interblockchain Communication](https://ibcprotocol.org/) protocol.
* [interBTC](https://github.com/interlay/interbtc) - Trustless and fully decentralized Bitcoin bridge to Polkadot and Kusama.
* [Joystream](https://github.com/Joystream/joystream) - A user governed video platform.
* [Lighthouse](https://github.com/sigp/lighthouse) - Rust Ethereum Consensus Layer (CL) Client.
* [madara](https://github.com/keep-starknet-strange/madara) - Kaioshin is a blazing fast Starknet sequencer, based on substrate and written in Rust.
* [mev-inspect-rs](https://github.com/flashbots/mev-inspect-rs) - Ethereum MEV Inspector in Rust.
* [nearcore](https://github.com/near/nearcore) - decentralized smart-contract platform for low-end mobile devices.
* [Nervos CKB](https://github.com/nervosnetwork/ckb) - Nervos CKB is a public permissionless blockchain, the common knowledge layer of Nervos network.
* [Nimiq](https://github.com/nimiq/core-rs) - Rust implementation of Nimiq node.
* [opensea-rs](https://github.com/gakonst/opensea-rs) - Rust bindings & CLI to the Opensea API and Contracts.
* [Parity-Bitcoin](https://github.com/paritytech/parity-bitcoin) - The Parity Bitcoin client.
* [Parity-Bridge](https://github.com/paritytech/parity-bridge) - Bridge between any two ethereum-based networks.
* [Parity-Ethereum](https://github.com/paritytech/parity-ethereum) - Fast, light, and robust Ethereum client.
* [Parity-Zcash](https://github.com/paritytech/parity-zcash) - Rust implementation of the Zcash protocol.
* [phala-blockchain](https://github.com/Phala-Network/phala-blockchain) — Confidential smart contract blockchain based on Intel SGX and Substrate.
* [polkadot-sdk](https://github.com/paritytech/polkadot-sdk) - The Parity Polkadot Blockchain SDK.
* [rbtc](https://github.com/lucawen/rbtc) - Convert BTC to any currency and vice-versa.
* [revm](https://github.com/bluealloy/revm) - Revolutionary Machine (revm) is a fast Ethereum virtual machine written in rust.
* [rust-bitcoin](https://github.com/rust-bitcoin/rust-bitcoin) - Library with support for de/serialization, parsing and executing on data structures and network messages related to Bitcoin.
* [rust-cardano](https://github.com/input-output-hk/rust-cardano) - Rust implementation of Cardano primitives, helpers, and related applications.
* [rust-lightning](https://github.com/lightningdevkit/rust-lightning) — Bitcoin Lightning library written in Rust. The main crate,`lightning`, does not handle networking, persistence, or any other I/O. Thus,it is runtime-agnostic, but users must implement basic networking logic, chain interactions, and disk storage.po on linking crate.
* [sigma-rust](https://github.com/ergoplatform/sigma-rust) - Rust implementation of ErgoTree interpreter and wallet-related features.
* [solana](https://github.com/solana-labs/solana) - Web-Scale Blockchain for fast, secure, scalable, decentralized apps and marketplaces.
* [Subspace](https://github.com/subspace/subspace) - The first layer-one blockchain that can fully resolve the blockchain trilemma by simultaneously achieving scalability, security, and decentralization.
* [Substrate](https://github.com/paritytech/substrate) - Generic modular blockchain template written in Rust.
* [Sui](https://github.com/MystenLabs/sui) - A next-generation smart contract platform with high throughput, low latency, and an asset-oriented programming model powered by the Move programming language.
* [svm-rs](https://github.com/roynalnaruto/svm-rs) - Solidity-Compiler Version Manager.
* [tendermint-rs](https://github.com/informalsystems/tendermint-rs) - Rust implementation of Tendermint blockchain data structures and clients.
* [wagu](https://crates.io/crates/wagu) - Generate a wallet for any cryptocurrency.
* [wagyu](https://github.com/howardwu/wagyu) - Rust library for generating cryptocurrency wallets.
* [zcash](https://github.com/zcash/zcash) — Zcash is an implementation of the "Zerocash" protocol.

### Database

* [Atomic-Server](https://github.com/atomicdata-dev/atomic-server/) - NoSQL graph database with realtime updates, dynamic indexing and easy-to-use GUI for CMS purposes.
* [CeresDB](https://github.com/CeresDB/ceresdb) - CeresDB is a high-performance, distributed, schema-less, cloud native time-series database that can handle both time-series and analytics workloads.
* [cnosdb](https://github.com/cnosdb/cnosdb) - An Open Source Distributed Time Series Database with high performance, high compression ratio and high usability.
* [CozoDb](https://github.com/cozodb/cozo) - A general-purpose, transactional, relational database that uses Datalog for query, is embeddable, and focuses on graph data and algorithms.
* [darkbird](https://github.com/Rustixir/darkbird) - HighConcurrency, RealTime, InMemory storage inspired by erlang mnesia.
* [Databend](https://github.com/datafuselabs/databend) -  A Modern Real-Time Data Processing & Analytics DBMS with Cloud-Native Architecture.
* [DB3 Network](https://github.com/dbpunk-labs/db3) - DB3 is a community-driven blockchain layer2 decentralized database network.
* [DragonMap](https://github.com/matobet/dragon-map) - A module for Redis adding support for 2nd level indexes over keys, supporting individual keys' expiration and automatic index grooming.
* [FnckSQL](https://github.com/KipData/FnckSQL) - SQL as a Function for Rust.
* [Garage](https://github.com/deuxfleurs-org/garage) - S3-compatible distributed object storage service designed for self-hosting at a small-to-medium scale.
* [GreptimeDB](https://github.com/grepTimeTeam/greptimedb) - An open-source, cloud-native, distributed time-series database with PromQL/SQL/Python supported.
* [indradb](https://crates.io/crates/indradb) - Rust based graph database.
* [lancedb](https://github.com/lancedb/lancedb) - A serverless, low-latency vector database for AI applications.
* [libmdbx-rs](https://github.com/vorot93/libmdbx-rs) - Rust bindings for MDBX, a "fast, compact, powerful, embedded, transactional key-value database, with permissive license". This is a fork of mozilla/lmdb-rs with patches to make it work with libmdbx.
* [Lucid](https://github.com/lucid-kv/lucid) - High performance and distributed KV store accessible through a HTTP API.
* [LocustDB](https://github.com/cswinter/LocustDB) - LocustDB is an experimental, single-node engine for parallel execution of analytical queries.
* [Materialize](https://github.com/MaterializeInc/materialize) - Streaming SQL database powered by Timely Dataflow.
* [native_db](https://github.com/vincent-herlemont/native_db) - Drop-in, embedded database for multi-platform apps (server, desktop, mobile). Sync Rust types effortlessly.
* [Neon](https://github.com/neondatabase/neon) - Neon is a serverless open-source alternative to AWS Aurora Postgres. It separates storage and compute and substitutes the PostgreSQL storage layer by redistributing data across a cluster of nodes.
* [noria](https://crates.io/crates/noria) - Dynamically changing, partially-stateful data-flow for web application backends.
* [oasysdb](https://github.com/oasysai/oasysdb) - An embedded vector database designed to run on the edge. Lightweight and fast with HNSW indexing algorithm.
* [oxigraph](https://github.com/oxigraph/oxigraph) - Oxigraph is a graph database implementing the [SPARQL](https://www.w3.org/TR/sparql11-overview/) standard.
* [ParadeDB](https://github.com/paradedb/paradedb) - ParadeDB is an Elasticsearch alternative built on Postgres, designed for real-time search and analytics.
* [ParityDB](https://github.com/paritytech/paritydb) - Fast and reliable database, optimised for read operation.
* [PumpkinDB](https://github.com/PumpkinDB/PumpkinDB) - an event sourcing database engine.
* [Qdrant](https://github.com/qdrant/qdrant) - An open source vector similarity search engine with extended filtering support.
* [qmdb](https://github.com/LayerZero-Labs/qmdb) - Quick Merkle Database (QMDB) is a high-performance verifiable key-value store, designed to optimize blockchain state storage.
* [qrlew](https://github.com/Qrlew/qrlew) - The SQL-to-SQL Differential Privacy layer.
* [Redis Oxide](https://github.com/dpbriggs/redis-oxide) - A multi-threaded implementation of redis written in rust.
* [RisingWave](https://github.com/RisingWaveLabs/risingwave) - The next-generation streaming database in the cloud.
* [rsedis](https://github.com/seppo0010/rsedis) - A Redis reimplementation in Rust.
* [rxqlite](https://github.com/HaHa421/rxqlite) - Distributed SQLite.
* [Skytable](https://github.com/skytable/skytable) - A multi-model NoSQL database.
* [sled](https://github.com/spacejam/sled) - A (beta) modern embedded database.
* [SQLSync](https://github.com/orbitinghail/sqlsync) - Multiplayer offline-first SQLite.
* [SurrealDB](https://github.com/surrealdb/surrealdb) - A scalable, distributed, document-graph database.
* [tectonicdb](https://github.com/0b01/tectonicdb) - Fast, highly compressed standalone database and streaming protocol for order book ticks.
* [TerminusDB](https://github.com/terminusdb/terminusdb-store) - open source graph database and document store
* [TerrabaseDB](https://github.com/terrabasedb/terrabasedb) - A multi-model NoSQL database
* [tikv](https://github.com/tikv/tikv) - A distributed KV database in Rust.
* [toydb](https://github.com/erikgrinaker/toydb) - Distributed SQL database in Rust, written as a learning project.
* [USearch](https://github.com/unum-cloud/usearch) - Similarity Search Engine for Vectors and Strings.
* [WooriDB](https://github.com/naomijub/wooridb) - General purpose time serial database inspired by Crux and Datomic.

### Emulators

See also [crates matching keyword 'emulator'](https://crates.io/keywords/emulator).

* Commodore 64
  * [rust64](https://github.com/kondrak/rust64)
* Flash Player
  * [Ruffle](https://github.com/ruffle-rs/ruffle) — Ruffle is an Adobe Flash Player emulator written in the Rust programming language. Ruffle targets both the desktop and the web using WebAssembly.  
* Gameboy
  * [mooneye-gb](https://github.com/Gekkio/mooneye-gb)
  * [rboy](https://github.com/mvdnes/rboy)
  * [RGB](https://github.com/nivent/RGB)
  * [gameboy](https://github.com/mohanson/gameboy) - Full featured Cross-platform GameBoy emulator. Forever boys!.
* GameMaker Advance  
  * [rustboyadvance-ng](https://github.com/michelhe/rustboyadvance-ng) - RustboyAdvance-ng is a Gameboy Advance emulator with desktop, android and [WebAssembly](https://michelhe.github.io/rustboyadvance-ng/) support.  
* GameMaker
  * [OpenGMK](https://github.com/OpenGMK/OpenGMK) — OpenGMK is a modern rewrite of the proprietary GameMaker Classic engines, providing a full sourceport of the runner, a decompiler, a TASing framework, and libraries for working with gamedata yourself.  
* NES
  * [oxidenes](https://github.com/iamsix/oxidenes)
  * [pinky](https://github.com/koute/pinky)
  * [pcwalton/sprocketnes](https://github.com/pcwalton/sprocketnes)
* Playstation
  * [rustation](https://github.com/simias/rustation)
* ZX Spectrum
  * [rustzx](https://github.com/pacmancoder/rustzx)
* Virtual Boy
  * [rustual-boy](https://github.com/emu-rs/rustual-boy)
* Intel 8080 CPU
  * [i8080](https://github.com/mohanson/i8080) - Intel 8080 cpu emulator by Rust
* iOS
  * [touchHLE](https://github.com/touchHLE/touchHLE) - High-level emulator for iPhone OS apps
* iPod
  * [clicky](https://github.com/daniel5151/clicky) - A clickwheel iPod emulator (WIP)  
* Emulator Development tools
  * SNES
    * [snesutilities](https://github.com/ioncodes/snesutilities) - ROM analyser/extractor

### File manager

* [broot](https://github.com/Canop/broot) - A new way to see and navigate directory trees (get an overview of a directory, even a big one; find a directory then `cd` to it; never lose track of file hierarchy while you search; manipulate your files, ...), further reading [dystroy.org/broot](https://dystroy.org/broot/)
* [joshuto](https://github.com/kamiyaa/joshuto) - ranger-like terminal file manager written in Rust.
* [xplr](https://github.com/sayanarijit/xplr) - A hackable, minimal, fast TUI file explorer.
* [yazi](https://github.com/sxyazi/yazi) - Blazing fast terminal file manager written in Rust, based on async I/O.

### Games

See also [Games Made With Piston](https://github.com/PistonDevelopers/piston/wiki/Games-Made-With-Piston).

* [angolmois-rust](https://github.com/lifthrasiir/angolmois-rust) - A minimalistic music video game which supports the BMS format.
* [chess-tui](https://github.com/thomas-mauran/chess-tui) -  A Chess TUI implementation in rust.
* [citybound](https://github.com/citybound/citybound) - The city sim you deserve.
* [Hyperspeedcube](https://github.com/HactarCE/Hyperspeedcube) - A modern, beginner-friendly 3D and 4D Rubik's cube simulator with customizable mouse and keyboard controls and advanced features for speedsolving
* [ludusavi](https://github.com/mtkennerly/ludusavi) - Backup tool for PC game saves.
* [sokoban-rs](https://github.com/swatteau/sokoban-rs) - A Sokoban implementation
* [Zone of Control](https://github.com/ozkriff/zoc) - A turn-based hexagonal strategy game.
* [rhex](https://github.com/dpc/rhex) - hexagonal ascii roguelike
* [zemeroth](https://github.com/ozkriff/zemeroth) - A small 2D turn-based hexagonal strategy game.
* [magog](https://github.com/rsaarelm/magog) - A roguelike game in Rust
* [rsnake](https://github.com/schulke-214/rsnake) - Snake written in Rust.
* [rsnake](https://github.com/maras-archive/rsnake) - Snake written in Rust.
* [rust-doom](https://github.com/cristicbz/rust-doom) - A renderer for Doom, may progress to being a playable game.
* [rust-quake](https://github.com/Thinkofname/rust-quake) - Quake map renderer in Rust
* [TGWM](https://github.com/aleshaleksey/TGWM) - An RPG with turned-based mechanics (work in progress).
* [rusted-ruins](https://github.com/garkimasera/rusted-ruins) - Extensible open world rogue like game with pixel art.
* [thetawave-legacy](https://github.com/thetawavegame/thetawave-legacy) - A space shooter game that strives to be an entry point for new game developers to make their first contributions.
* [Veloren](https://gitlab.com/veloren/veloren) - An open-world, open-source multiplayer voxel RPG.

### Graphics

* [euclider](https://github.com/Limeth/euclider) - A real-time 4D CPU ray tracer.
* [papercraft](https://github.com/rodrigorc/papercraft) - A tool to unwrap 3D models and create them in paper with scissors and glue.
* [resvg](https://github.com/RazrFalcon/resvg) - An SVG rendering library.
* [rust-raytracer](https://github.com/dps/rust-raytracer) - An implementation of a very simple raytracer based on Ray Tracing in One Weekend by Peter Shirley in Rust.
* [rustracer](https://github.com/KaminariOS/rustracer) — A PBR glTF 2.0 renderer based on Vulkan ray-tracing, written in Rust.
* [svgbob](https://github.com/ivanceras/svgbob) - converts ASCII diagrams into SVG graphics.
* [svgcleaner](https://github.com/RazrFalcon/svgcleaner) - tidies SVG graphics
* [tray_rust](https://github.com/Twinklebear/tray_rust) - A ray tracer.
* [rs_pbrt](https://github.com/wahn/rs_pbrt) - Rust crate to implement a counterpart to the PBRT book's (3rd edition) C++ code.
* [vue-skia](https://github.com/rustq/vue-skia) - Skia based 2d graphics vue rendering library. It is based on Rust to implement software rasterization to perform rendering.
* Image processing:
  * [Image-Processing-CLI-in-Rust](https://github.com/spejss/Image-Processing-CLI-in-Rust) - CLI for processing images, generating histograms.

### Industrial automation

* [modbus-iiot-rust](https://github.com/BiancoRoyal/modbus-iiot-rust) - A pure rust [modbus](https://modbus.org) library with no or less dependencies.
* [opcua](https://github.com/locka99/opcua) - A pure rust [OPC UA](https://opcfoundation.org/about/opc-technologies/opc-ua/) library.
* [tokio-modbus](https://github.com/slowtec/tokio-modbus) - A [tokio](https://tokio.rs)-based [modbus](https://modbus.org) library.

### Observability

* [bioyino](https://github.com/avito-tech/bioyino) — A high-performance scalable StatsD compatible server.
* [gtsa](https://github.com/Mnwa/gtsa) - A simple solution to proxy gelf messages (messages for Graylog) to Sentry.
* [OpenObserve](https://github.com/openobserve/openobserve) - OpenObserve is a cloud native observability platform built specifically for logs, metrics, traces and analytics designed to work at petabyte scale.
* [quickwit](https://github.com/quickwit-oss/quickwit) - Cloud-native and highly cost-efficient search engine for log management.
* [Scaphandre](https://github.com/hubblo-org/scaphandre) - A power consumption monitoring agent, to track host and each service power consumption and enable designing systems and applications for more sustainability. Designed to fit any monitoring toolchain (already supports prometheus, warp10, riemann...).
* [vector](https://github.com/vectordotdev/vector) - A High-Performance, Logs, Metrics, & Events Router.

### Operating systems

See also [A comparison of operating systems written in Rust](https://github.com/flosse/rust-os-comparison).

* [redox](https://gitlab.redox-os.org/redox-os/redox)
* [rust_os](https://github.com/thepowersgang/rust_os)
* [SteinsOS](https://github.com/0x59616e/SteinsOS) - An OS for armv8-a architecture.
* [Theseus](https://github.com/theseus-os/Theseus) - A safe-language, single address space and single privilege level OS written from scratch in pure Rust.
* [tock](https://github.com/tock/tock) - A secure embedded operating system for Cortex-M based microcontrollers
* [nebulet](https://github.com/nebulet/nebulet) - A microkernel that implements a WebAssembly "usermode" that runs in Ring 0.

### Productivity

* [ast-grep](https://github.com/ast-grep/ast-grep) - A CLI tool for code structural search, lint and rewriting. Written in Rust.
* [Bartib](https://github.com/nikolassv/bartib) - A simple timetracker for the command line.
* [espanso](https://github.com/federico-terzi/espanso) - A cross-platform Text Expander written in Rust.
* [eureka](https://crates.io/crates/eureka) - A CLI tool to input and store your ideas without leaving the terminal.
* [Furtherance](https://github.com/lakoliu/Furtherance) - Time tracking app built with Rust and GTK4.
* [illa](https://github.com/illacloud/illa) [[ILLA Cloud](https://www.illacloud.com/)] - Low-code internal tool builder written with Rust.
* [LLDAP](https://github.com/lldap/lldap) - Simplified LDAP interface for authentication.
* [pier](https://github.com/pier-cli/pier) - A central repository to manage (add, search metadata, etc.) all your one-liners, scripts, tools, and CLIs.
* [rust_kanban](https://github.com/yashs662/rust_kanban) - A Kanban App for the terminal.
* [subilo](https://github.com/huemul/subilo) - A continuous deployment agent.
* [work-break](https://github.com/ShadoySV/work-break) - Work and rest time balancer taking into account your current and today strain.

### Routing protocols

* [Holo](https://github.com/rwestphal/holo) - Holo is a suite of routing protocols designed to support high-scale and automation-driven networks.
* [RustyBGP](https://github.com/osrg/rustybgp) - BGP implemented in the Rust Programming Language.

### Security tools

* [abcdict](https://github.com/b23r0/abcdict) - A better customization password dictionary generator.
* [arp-scan-rs](https://github.com/kongbytes/arp-scan-rs) - A minimalistic ARP scan tool for fast local network scans.
* [badtouch](https://github.com/kpcyrd/badtouch) - A scriptable network authentication cracker.
* [Cherrybomb](https://github.com/blst-security/cherrybomb) - Stop half-done API specifications with a CLI tool that helps you avoid undefined user behaviour by validating your API specifications.
* [Cliws](https://github.com/b23r0/Cliws) - A bind/reverse PTY shell with Windows & Linux support.
* [feroxbuster](https://github.com/epi052/feroxbuster) - A simple, fast, recursive content discovery tool written in Rust.
* [inspektor](https://github.com/poonai/inspektor) - A database protocol-aware proxy that is used to enforce access policies.
* [lethe](https://github.com/kostassoid/lethe) - A secure cross-platform drive wiping utility.
* [LibAFL](https://github.com/AFLplusplus/LibAFL) - Advanced Fuzzing Library - Slot your Fuzzer together in Rust! Scales across cores and machines. For Windows, Android, MacOS, Linux, no_std, etc.
* [libinjection-rs](https://github.com/arvancloud/libinjection-rs) - Rust bindings for [libinjection](https://github.com/client9/libinjection).
* [ObserverWard](https://github.com/0x727/ObserverWard) - Cross platform community web fingerprint identification tool.
* [Raspirus](https://github.com/Raspirus/Raspirus) - User- and resources-friendly signatures-based malware scanner.
* [ripasso](https://github.com/cortex/ripasso/) - A password manager, filesystem compatible with pass.
* [rshijack](https://github.com/kpcyrd/rshijack) - A TCP connection hijacker, rust rewrite of shijack.
* [rsocx](https://github.com/b23r0/rsocx) - A bind/Reverse Socks5 proxy Server.
* [rustscan](https://github.com/RustScan/RustScan) - Make Nmap faster with this port scanning tool.
* [sniffglue](https://github.com/kpcyrd/sniffglue) - A secure multithreaded packet sniffer.
* [sn0int](https://github.com/kpcyrd/sn0int) - A semi-automatic OSINT framework and package manager.
* [anevicon](https://github.com/Gymmasssorla/anevicon) - The most powerful UDP-based load generator, written in Rust.
* [finshir](https://github.com/Gymmasssorla/finshir) - A coroutines-driven Low & Slow traffic generator, written in Rust.

### Social networks

* [Rustodon](https://github.com/rustodon/rustodon) - A Mastodon-compatible, ActivityPub-speaking server in Rust.

### System tools

* [atuin](https://github.com/ellie/atuin) - Atuin replaces your existing shell history with a SQLite database, and records additional context for your commands. Additionally, it provides optional and fully encrypted synchronisation of your history between machines, via an Atuin server.
* [bat](https://github.com/sharkdp/bat) - A cat(1) clone with wings.
* [bingrep](https://github.com/m4b/bingrep) - Greps through binaries from various OSs and architectures, and colors them.
* [bottom](https://github.com/ClementTsang/bottom) - Yet another cross-platform graphical process/system monitor.
* [bustd](https://github.com/vrmiguel/bustd) - Lightweight process killer daemon to handle out-of-memory scenarios on Linux.
* [cicada](https://github.com/mitnk/cicada) - A bash-like Unix shell.
* [cocom](https://github.com/LamdaLamdaLamda/cocom) - NTP client purely written in Rust.
* [concurr](https://github.com/mmstick/concurr) - Alternative to GNU Parallel w/ a client-server architecture.
* [coreutils](https://github.com/uutils/coreutils) - A cross-platform Rust rewrite of the GNU coreutils.
* [crabz](https://github.com/sstadick/crabz) - Multi-threaded compression and decompression CLI tool.
* [czkawka](https://github.com/qarmin/czkawka) - Multi-functional app to find duplicates, empty folders, similar images, etc.
* [debrep](https://github.com/pop-os/debrepbuild) - APT repository tool for building and managing an APT repo.
* [ddh](https://github.com/darakian/ddh) - Fast duplicate file finder.
* [eza](https://github.com/eza-community/eza) - A replacement for 'ls'.
* [fblog](https://github.com/brocode/fblog) - Small command-line JSON Log viewer.
* [fclones](https://github.com/pkolaczk/fclones) - Efficient duplicate file finder and remover.
* [findex](https://github.com/mdgaziur/findex) - Findex is a highly customizable application finder written in Rust and uses GTK3.
* [fuc](https://github.com/supercilex/fuc) - Fast `cp` and `rm` commands.
* [funzzy](https://github.com/cristianoliveira/funzzy) - A configurable filesystem watcher inspired by [entr](http://entrproject.org/).
* [fontfinder](https://github.com/mmstick/fontfinder) - GTK3 application for previewing and installing Google's fonts.
* [fselect](https://crates.io/crates/fselect) - Find files with SQL-like queries.
* [gfold](https://github.com/nickgerace/gfold) - CLI tool to help keep track of multiple Git repositories.
* [gitui](https://github.com/extrawurst/gitui) - Blazing fast terminal client for git written in Rust.
* [goto](https://github.com/slai11/goto) - A simple and user-friendly way to jump to your indexed directories.
* [GQL](https://github.com/amrdeveloper/gql) - A SQL like query language to run on .git files.
* [httm](https://github.com/kimono-koans/httm) - Interactive, file-level Time Machine-like tool for ZFS/btrfs/nilfs2 (and even actual Time Machine backups!)
* [kickoff](https://github.com/j0ru/kickoff) - Fast and snappy wayland program launcher.
* [Kondo](https://github.com/tbillington/kondo) - CLI & GUI tool for deleting software project artifacts and reclaiming disk space.
* [logram](https://github.com/mxseev/logram) - Push log files' updates to Telegram.
* [jnv](https://github.com/ynqa/jnv) - Interactive JSON filter using jq.
* [netscanner](https://github.com/Chleba/netscanner) - TUI Network Scanner.
* [nushell](https://github.com/nushell/nushell) - A new type of shell.
* [rrun](https://github.com/buster/rrun) - A command launcher for Linux, similar to gmrun.
* [procs](https://github.com/dalance/procs) - A modern replacement for 'ps' written by Rust.
* [zou](https://github.com/k0pernicus/zou) - A download accelerator.
* [rargs](https://github.com/lotabout/rargs) - xargs + awk with pattern matching support.
* [skim](https://github.com/lotabout/skim) - A fuzzy finder in pure rust.
* [LACT](https://github.com/ilya-zlobintsev/LACT) - Linux AMDGPU Controller
* [lsd](https://github.com/lsd-rs/lsd) — An ls with a lot of pretty colors and awesome icons
* [parallel](https://github.com/mmstick/parallel) - Reimplementation of GNU Parallel
* [systemd-manager](https://github.com/mmstick/systemd-manager) - A systemd service manager written in Rust using GTK-rs.
* [tv-renamer](https://github.com/mmstick/tv-renamer) - A tv series renaming application with an optional GTK3 frontend.
* [lsd](https://github.com/Peltoche/lsd) - An ls with a lot of pretty colors and awesome icons.
* [ouch](https://github.com/ouch-org/ouch) - Painless compression and decompression on the command-line.
* [popsicle](https://github.com/pop-os/popsicle) - GTK3 & CLI utility for flashing multiple USB devices in parallel.
* [pueue](https://github.com/nukesor/pueue) - Manage your long running shell commands.
* [rip](https://github.com/nivekuil/rip) - A safe and ergonomic alternative to `rm`
* [sig](https://github.com/ynqa/sig) - Interactive grep (for streaming).
* [synapse](https://github.com/Luminarys/synapse) - Flexible and fast BitTorrent daemon.
* [system76-power](https://github.com/pop-os/system76-power/) - Linux power management daemon (DBus-interface) with CLI tool.
* [trippy](https://github.com/fujiapple852/trippy) - A network diagnostic tool.
* [logram](https://github.com/Ralvke/logram) - Push log files' updates to Telegram.
* [ion](https://github.com/redox-os/ion) - Next-generation system shell.
* [fd](https://github.com/sharkdp/fd) - A simple, fast and user-friendly alternative to find.
* [hex](https://github.com/sitkevij/hex) - A colorized hexdump terminal utility.
* [watchexec](https://github.com/watchexec/watchexec) - Executes commands in response to file modifications.
* [XAMPPRocky/tokei](https://github.com/XAMPPRocky/tokei) - counts the lines of code.
* [yake](https://crates.io/crates/yake) - Yake is a task runner based on yaml files.
* [ytop](https://github.com/cjbassi/ytop) - A TUI system monitor written in Rust.
* [Voila](https://github.com/Alonely0/Voila) - Voila is a domain-specific language launched through CLI tool for operating with files and directories in massive amounts in a fast & reliable way.

### Text editors

* [amp](https://amp.rs) - Inspired by Vi/Vim.
* [helix](https://github.com/helix-editor/helix) - A post-modern modal text editor inspired by Neovim/Kakoune.
* [iota](https://github.com/gchp/iota) - A simple text editor.
* [kibi](https://github.com/ilai-deutel/kibi) - A tiny (≤1024 LOC) text editor with syntax highlighting, incremental search and more.
* [lapce](https://github.com/lapce/lapce) - Lightning-fast and Powerful Code Editor written in Rust.
* [ox](https://github.com/curlpipe/ox) - An independent Rust text editor that runs in your terminal
* [pepper](https://github.com/vamolessa/pepper) - An opinionated modal editor to simplify code editing from the terminal
* [Remacs](https://github.com/remacs/remacs) - A community-driven port of Emacs to Rust.
* [rim](https://github.com/mathall/rim) - Vim-like text editor written in Rust.
* [xi-editor](https://github.com/xi-editor/xi-editor) - A modern editor with a backend written in Rust.
* [xray](https://github.com/atom/xray) - An experimental next-generation Electron-based text editor.
* [zed](https://github.com/zed-industries/zed) - A high-performance, multiplayer code editor from the creators of Atom and Tree-sitter.

### Text processing

* [bytelines](https://github.com/whitfin/bytelines) - Read input lines as byte slices for high efficiency.
* [cfonts](https://github.com/dominikwilkowski/cfonts) - Sexy ANSI fonts for the console.
* [cpc](https://github.com/probablykasper/cpc) - Parses and calculates strings of math with support for units and unit conversion, from `1+2` to `1% of round(1 lightyear / 14!s to km/h)`.
* [ff](https://github.com/vishaltelangre/ff) - Find files (ff) by name!
* [grex](https://github.com/pemistahl/grex) - A command-line tool and library for generating regular expressions from user-provided test cases.
* [Melody](https://github.com/yoav-lavi/melody) - A language that compiles to regular expressions and aims to be more easily readable and maintainable.
* [ruplacer](https://github.com/your-tools/ruplacer) - Find and replace text in source files.
* [ripgrep](https://crates.io/crates/ripgrep) - combines the usability of The Silver Searcher with the raw speed of grep.
* [ripgrep-all](https://github.com/phiresky/ripgrep-all) - ripgrep, but also search in PDFs, E-Books, Office documents, zip, tar.gz, etc.
* [sd](https://crates.io/crates/sd) - Intuitive find & replace CLI.
* [todo_r](https://github.com/lavifb/todo_r) - Find all your TODO notes with one command!
* [runiq](https://github.com/whitfin/runiq) - an efficient way to filter duplicate lines from unsorted input.
* [Shiva](https://github.com/igumnoff/shiva) - Shiva library: Implementation in Rust of a parser and generator for documents of any type. [docs.rs/shiva](https://docs.rs/shiva)
* [so_stupid_search](https://github.com/Lisprez/so_stupid_search) - A simple and fast string search tool for human beings.
* [qsv](https://github.com/jqnatividad/qsv) - A high performance CSV data-wrangling toolkit. Forked from xsv, with 34+ additional commands & more.
* [xsv](https://crates.io/crates/xsv) - A fast CSV command line tool (slicing, indexing, selecting, searching, sampling, etc.)

### Utilities

* [1History](https://github.com/1History/1History) - Command line interface to backup Firefox/Chrome/Safari history to one SQLite file.
* [AZDice](https://github.com/aleshaleksey/AZDice) - A dice roll success distribution generator for tabletop homebrewers.
* [barberousse](https://github.com/zeapo/barberousse) - AWS Secrets Manager editor.
* [cb](https://github.com/yaa110/cb) - Command line interface to manage clipboard.
* [checkpwn](https://github.com/brycx/checkpwn) - A Have I Been Pwned (HIBP) command-line utility tool that lets you easily check for compromised accounts and passwords.
* [cloak](https://github.com/evansmurithi/cloak) - A Command Line OTP (One Time Password) Authenticator application.
* [consoletimer](https://github.com/arthrp/consoleTimer) - Simple timer for your terminal.
* [cotp](https://github.com/replydev/cotp) - Trustworthy encrypted one time password authenticator app compatible with external backups.
* [dtool](https://github.com/guoxbin/dtool) - A useful command-line tool collection to assist development including conversion, codec, hashing, encryption, etc.
* [Epic Asset Manager](https://github.com/AchetaGames/Epic-Asset-Manager) — An unofficial client to install Unreal Engine, download and manage purchased assets, projects, plugins and games from the Epic Games Store.
* [flawz](https://github.com/orhun/flawz) - A Terminal UI for browsing security vulnerabilities (CVEs).
* [freepass](https://github.com/myfreeweb/freepass) - The free password manager for power users.
* [gitweb](https://github.com/yoannfleurydev/gitweb) - Open the current remote repository in your browser.
* [mdBook](https://github.com/rust-lang/mdBook) - mdBook is a utility to create modern online books from Markdown files.
* [mme](https://github.com/GoberInfinity/mme) - Command line tool to remember you commands that you sometimes forget.
* [Mobslide](https://github.com/thewh1teagle/mobslide) - Desktop application that turns your smartphone into presentation remote controller.
* [mprocs](https://github.com/pvolok/mprocs) — TUI for running multiple processes.
* [nix-init](https://github.com/nix-community/nix-init) - Generate Nix packages from URLs with hash prefetching, dependency inference, license detection, and more.
* [nix-melt](https://github.com/nix-community/nix-melt) - A ranger-like flake.lock viewer.
* [nurl](https://github.com/nix-community/nurl) - Generate Nix fetcher calls from repository URLs.
* [nomino](https://github.com/yaa110/nomino) - Batch rename utility for developers.
* [oxker](https://github.com/mrjackwills/oxker) - A simple tui to view & control docker containers.
* [quick-skeleton](https://github.com/arthrp/quick-skeleton) - Project scaffolding tool, similar to Yeoman and Slush.
* [rage](https://github.com/str4d/rage) - Rust implementation of [age](https://github.com/FiloSottile/age).
* [rubigo](https://github.com/yaa110/rubigo) - Golang dependency tool and package manager, written in Rust.
* [s3-concat](https://github.com/whitfin/s3-concat) - A command line tool to concatenate Amazon S3 files remotely using flexible patterns.
* [s3-meta](https://github.com/whitfin/s3-meta) - A command line tool to gather metadata about your Amazon S3 buckets.
* [tmux-thumbs](https://github.com/fcsonline/tmux-thumbs) - A lightning fast version of tmux-fingers written in Rust, copy/pasting tmux like vimium/vimperator.
* [repoch](https://github.com/lucawen/repoch) - Convert epoch to datetime and datetime to epoch.
* [rewrk](https://github.com/lnx-search/rewrk) - A more modern http framework benchmarker supporting HTTP/1 and HTTP/2 benchmarks.
* [suckit](https://github.com/Skallwar/suckit) - Recursively visit and download a website's content to your disk.
* [Tabiew](https://github.com/shshemi/tabiew) - A lightweight TUI app to view and query CSV files.
* [Vibe](https://github.com/thewh1teagle/vibe) - Transcribe audio or video in every language on every platform.
* [Warp](https://github.com/warpdotdev/Warp) - Warp is a blazingly-fast modern Rust based GPU-accelerated terminal built to make you and your team more productive.
* [workstation](https://github.com/amar-laksh/workstation) - A commandline tool to help you manage your workstation by distancing you from your screen, locking your screen when you aren't there among other things with OPENCV!
* [wrestic](https://github.com/alvaro17f/wrestic) - A wrapper around restic built in rust.
* [wthrr](https://github.com/ttytm/wthrr-the-weathercrab) - Weather companion for the terminal.

### Unsafe

* [zerocopy](https://crates.io/crates/zerocopy) - "Zerocopy makes zero-cost memory manipulation effortless. We write `unsafe` so you don’t have to."

### Video

* [camera_capture](https://github.com/oli-obk/camera_capture) - Webcam capturing in Rust, captures webcam images and offers access to them through an iterator.
* [gyroflow](https://github.com/gyroflow/gyroflow) - Video stabilization application using gyroscope data.
* [slingr](https://github.com/yuvadm/slingr) - A simple CLI for streaming media files over a local network to UPnP media renderers.
* [streamlib](https://github.com/streamlib/streamlib) - Play your favorite live video and audio streams from command line.
* [vidmerger](https://github.com/TGotwig/vidmerger) - Merge video & audio files via CLI.
* [rav1e](https://github.com/xiph/rav1e) - The fastest and safest AV1 encoder.
* [yaydl](https://github.com/dertuxmalwieder/yaydl) - A simple video downloader

### Virtualization

* [firecracker](https://github.com/firecracker-microvm/firecracker) - A lightweight virtual machine for container workload  [Firecracker Microvm](https://firecracker-microvm.github.io/)
* [kata-containers](https://github.com/kata-containers/kata-containers) - A implementation of lightweight Virtual Machines (VMs) that feel and perform like containers, but provide the workload isolation and security advantages of VMs.
* [propolis](https://github.com/oxidecomputer/propolis) - Rust-based userspace program for illumos bhyve kernel modules.
* [railcar](https://github.com/oracle/railcar) - Docker-like container OCI runtime implementation in Rust.
* [vagga](https://github.com/tailhook/vagga) - A containerization tool without daemons.
* [wasmtime](https://github.com/bytecodealliance/wasmtime) - A standalone runtime for WebAssembly.
* [xiu](https://github.com/harlanc/xiu) - A powerful and secure live server by pure rust (rtmp/httpflv/hls/relay).

### Window managers

* [way-cooler](https://github.com/way-cooler/way-cooler) - A customizable Wayland compositor (window manager)

### Web

* [backend](https://github.com/revoltchat/backend) - User-first chat platform built with modern web technologies.
* [Bitwarden](https://github.com/dani-garcia/vaultwarden) - Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs.
* [delta](https://github.com/revoltchat/delta) - User-first chat platform built with modern web technologies.
* [lemmy](https://github.com/LemmyNet/lemmy) - A link aggregator / reddit clone for the fediverse.
* [Plume](https://github.com/Plume-org/Plume) - ActivityPub federating blogging application.
* [ruster](https://github.com/rustlang-cn/ruster) - online community in rust for rust community.
* [spin](https://github.com/fermyon/spin) - Spin is a framework for building, deploying, and running fast, secure, and composable cloud microservices with WebAssembly.
* [tobaru](https://github.com/cfal/tobaru) - Port forwarder with allowlists, IP and TLS SNI/ALPN rule-based routing, iptables support, round-robin forwarding (load balancing), and hot reloading.

### Web Servers

* [Frolic](https://github.com/frolicorg/frolic) - Frolic provides a ready made APIs to query all your data. This removes the need to write multiple APIs for different dashboard components and their database queries.
* [http](https://github.com/thecoshman/http) - Host These Things Please - A basic http server for hosting a folder fast and simply.
* [leptos](https://github.com/leptos-rs/leptos) - Leptos is a full-stack, isomorphic Rust web framework leveraging fine-grained reactivity to build declarative user interfaces.
* [meilisearch](https://github.com/meilisearch/meilisearch) - A lightning-fast search engine that fits effortlessly into your apps, websites, and workflow.
* [miniserve](https://github.com/svenstaro/miniserve) - A small, self-contained cross-platform CLI tool that allows you to just grab the binary and serve some file(s) via HTTP.
* [skyfolder](https://github.com/mu-arch/skyfolder) - Beautiful HTTP/Bittorrent server without the hassle. Secure - GUI - Pretty - Fast.
* [simple-http-server](https://github.com/TheWaWaR/simple-http-server) - simple static http server.
* [see](https://github.com/wyhaya/see) - Static file server.
* [rust-hasura](https://github.com/ronanyeah/rust-hasura) - A demonstration of how a Rust GraphQL server can be used as a remote schema with [Hasura](https://hasura.io/)
* [static-web-server](https://github.com/static-web-server/static-web-server) - A blazing fast and asynchronous web server for static files-serving.

## Development tools

* [ATAC](https://github.com/Julien-cpsn/ATAC) — A feature-full TUI API client made in Rust. ATAC is free, open-source, offline and account-less.
* [artifact](https://github.com/vitiral/artifact) - the design doc tool made for developers.
* [bacon](https://github.com/Canop/bacon) — background rust code checker, similar to cargo-watch.
* [clippy](https://crates.io/crates/clippy) - Rust lints.
* [clog-cli](https://github.com/clog-tool/clog-cli) - generates a changelog from git metadata.
* [comtrya](https://github.com/comtrya/comtrya) - A configuration management tool for localhost / dotfiles.
* [create-rust-app](https://github.com/Wulf/create-rust-app) - Set up a modern rust+react web app by running one command.
* [rusty-tags](https://github.com/dan-t/rusty-tags) - create ctags/etags for a cargo project and all of its dependencies.
* [datanymizer](https://github.com/datanymizer/datanymizer) - Powerful database anonymizer with flexible rules.
* [delta](https://crates.io/crates/git-delta) - A syntax-highlighter for git and diff output.
* [dotenv-linter](https://github.com/dotenv-linter/dotenv-linter) - Linter for `.env` files.
* [envio](https://github.com/envio-cli/envio) - A Modern And Secure CLI Tool For Managing Environment Variables.
* [crate-deps](https://github.com/frewsxcv/crate-deps) - generates images of dependency graphs for crates hosted on crates.io
* [fw](https://github.com/brocode/fw) - workspace productivity booster.
* [git-journal](https://github.com/saschagrunert/git-journal) - The Git Commit Message and Changelog Generation Framework.
* [hot-lib-reloader](https://github.com/rksm/hot-lib-reloader-rs) - Hot reload Rust code.
* [intelli-shell](https://github.com/lasantosr/intelli-shell) - Bookmark commands with placeholders and search or autocomplete at any timeю
* [just](https://github.com/casey/just) - A handy command runner for project-specific tasks.
* [mask](https://github.com/jakedeichert/mask) - A CLI task runner defined by a simple markdown file.
* [Module Linker](https://github.com/fiatjaf/module-linker) - Extension that adds `<a>` links to references in `mod`, `use` and `extern crate` statements at GitHub.
* [ptags](https://github.com/dalance/ptags) - A parallel universal-ctags wrapper for git repository.
* [Racer](https://github.com/racer-rust/racer) - code completion for Rust.
* [rustfix](https://github.com/rust-lang-nursery/rustfix) - automatically applies the suggestions made by rustc.
* [rust_echo_bench](https://github.com/haraldh/rust_echo_bench) - A simple rust echo server benchmark.
* [Rustup](https://github.com/rust-lang/rustup.rs) - the Rust toolchain installer.
* [Rust Language Server](https://github.com/rust-lang/rls) - A server that runs in the background, providing IDEs, editors, and other tools with information about Rust programs
* [semantic-rs](https://github.com/semantic-rs/semantic-rs) - automatic crate publishing.
* [synth](https://github.com/shuttle-hq/synth) - A declarative data generation engine.
* [tinyrick](https://github.com/mcandre/tinyrick) a basic task dependency tool emphasizing Rust functions over raw shell commands.

### Build system

* [Cargo](https://crates.io/) - the Rust package manager
  * [cargo-benchcmp](https://crates.io/crates/cargo-benchcmp) - A utility to compare Rust micro-benchmarks.
  * [cargo-bitbake](https://crates.io/crates/cargo-bitbake) - A cargo extension that can generate BitBake recipes utilizing the classes from meta-rust.
  * [cargo-cache](https://crates.io/crates/cargo-cache) - inspect/manage/clean your cargo cache (`~/.cargo/`/`${CARGO_HOME}`), print sizes etc.
  * [cargo-check](https://crates.io/crates/cargo-check) - A wrapper around `cargo rustc -- -Zno-trans` which can be helpful for running a faster compile if you only need correctness checks.
  * [cargo-commander](https://crates.io/crates/cargo-commander) — A subcommand for `cargo` to run CLI commands similar to how the scripts section in `package.json` works.
  * [cargo-count](https://crates.io/crates/cargo-count) - lists source code counts and details about cargo projects, including unsafe statistics.
  * [cargo-deb](https://crates.io/crates/cargo-deb) - Generates binary Debian packages.
  * [cargo-deps](https://crates.io/crates/cargo-deps) - build dependency graphs of Rust projects.
  * [cargo-do](https://crates.io/crates/cargo-do) - run multiple cargo commands in a row.
  * [cargo-ebuild](https://crates.io/crates/cargo-ebuild) - cargo extension that can generate ebuilds using the in-tree eclasses.
  * [cargo-edit](https://crates.io/crates/cargo-edit) - allows you to add and list dependencies by reading/writing to your Cargo.toml file from the command line.
  * [cargo-find](https://crates.io/crates/cargo-find) <sup>deprecated</sup> - Find crates from command line.
  * [cargo-generate](https://github.com/ashleygwilliams/cargo-generate) A generator of a rust project by leveraging a pre-existing git repository as a template.
  * [cargo-get](https://crates.io/crates/cargo-get) - Cargo plugin to easily query information from Cargo.toml files.
  * [cargo-graph](https://crates.io/crates/cargo-graph) - updated fork of `cargo-dot` with additional features. Unmaintained, see `cargo-deps`.
  * [cargo-info](https://crates.io/crates/cargo-info) - queries crates.io for crates details from command line.
  * [cargo-make](https://crates.io/crates/cargo-make) - Rust task runner and build tool.
  * [cargo-modules](https://crates.io/crates/cargo-modules) - A cargo plugin for showing a tree-like overview of a crate's modules.
  * [cargo-multi](https://crates.io/crates/cargo-multi) - runs specified cargo command on multiple crates.
  * [cargo-outdated](https://crates.io/crates/cargo-outdated) - displays when newer versions of Rust dependencies are available, or out of date.
  * [cargo-release](https://crates.io/crates/cargo-release) - tool for releasing git-managed cargo project, build, tag, publish, doc and push.
  * [cargo-script](https://crates.io/crates/cargo-script) - lets people quickly and easily run Rust "scripts" which can make use of Cargo's package ecosystem.
  * [cargo-testify](https://crates.io/crates/cargo-testify) - watches files changes, runs tests and notifies about the result with friendly OS notification.
  * [cargo-update](https://crates.io/crates/cargo-update) - cargo subcommand for checking and applying updates to installed executables.
  * [cargo-watch](https://crates.io/crates/cargo-watch) - utility for cargo to compile projects when sources change.
  * [cargo-x](https://github.com/liuchong/cargo-x) - A very simple third-party cargo subcommand to execute a custom command.
  * [cargo-expand](https://github.com/dtolnay/cargo-expand) - Expand macros in your source code
* CMake
  * [CMakeRust](https://github.com/Devolutions/CMakeRust) - useful for integrating a Rust library into a CMake project
  * [RustCMake](https://github.com/SiegeLord/RustCMake) - an example project showing usage of CMake with Rust.
* Github actions
  * [rust-action](https://github.com/icepuma/rust-action) - rust github action
* Webpack
  * [rust-loader](https://github.com/Ralvke/rust-loader) - Webpack Rust loader (wasm)

* [Fleet](https://github.com/dimensionhq/fleet) - The blazing fast build tool for Rust.

### Debugging

* GDB
  * [rust-gdb](https://github.com/rust-lang/rust/blob/master/src/etc/rust-gdb)
  * [gdbgui](https://github.com/cs01/gdbgui) - Browser based frontend for gdb to debug C, C++, Rust, and go.
* LLDB
  * [lldb_batchmode.py](https://github.com/rust-lang/rust/blob/master/src/etc/lldb_batchmode.py) - allows to use LLDB in a way similar to GDB's batch mode.
  * [CodeLLDB](https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb) - A LLDB extension for [Visual Studio Code](https://code.visualstudio.com/).

### Deployment

* Docker
  * [rust-musl-builder](https://github.com/emk/rust-musl-builder) - Docker images for compiling static Rust binaries using musl-libc and musl-gcc, with static versions of useful C libraries
  * [mini-docker-rust](https://github.com/kpcyrd/mini-docker-rust) - An example project for very small rust docker images.
  * [docker-rustup](https://github.com/liuchong/docker-rustup) - A multiple version (with musl tools) Rust Docker image.
  * [is_ready](https://github.com/Stavrospanakakis/is_ready) - Wait for multiple services to become available.
  * [rust-musl-cross](https://github.com/rust-cross/rust-musl-cross) - Docker images for compiling static Rust binaries using musl-cross.
  * [docker-rust](https://github.com/rust-lang/docker-rust) - the official Rust Docker image
* Google App Engine
  * [rust-app-engine](https://github.com/DenisKolodin/rust-app-engine) - App Engine Rust boilerplate
* Heroku
  * [heroku-buildpack-rust](https://github.com/emk/heroku-buildpack-rust) - A buildpack for Rust applications on Heroku.
* [release-plz](https://github.com/MarcoIeni/release-plz) - Release Rust crates from CI, with changelog generation and semver check.  

### Embedded

[Rust Embedded](https://rust-embedded.org/)

* Arduino
  * [ruduino](https://github.com/avr-rust/ruduino) ^`^t Reusable components for the Arduino Uno.
* Cross compiling
  * [rust-cross](https://github.com/japaric/rust-cross) - everything you need to know about cross compiling Rust programs.
  * [xargo](https://github.com/japaric/xargo) - effortless cross compilation of Rust programs to custom bare-metal targets like ARM Cortex-M.
* Espressif
  * [esp-rs](https://github.com/esp-rs) - home to a number of community projects enabling the use of the Rust programming language on various SoCs and modules produced by Espressif Systems.
* Firmware
  * [oreboot](https://github.com/oreboot/oreboot) - oreboot is a fork of coreboot, with C removed, written in Rust.
* nRF
  * [nrf-hal](https://github.com/nrf-rs/nrf-hal) - A Rust HAL for the nRF family of devices.
* Raspberry Pi
  * [rust-on-raspberry-pi](https://github.com/Ogeon/rust-on-raspberry-pi) - instructions for how to cross compile Rust projects for the Raspberry Pi .

### FFI

See also [Foreign Function Interface](https://doc.rust-lang.org/book/first-edition/ffi.html),  [The Rust FFI Omnibus](http://jakegoulding.com/rust-ffi-omnibus/) (a collection of examples of using code written in Rust from other languages) and [FFI examples written in Rust](https://github.com/alexcrichton/rust-ffi-examples).

* C
  * [cbindgen](https://github.com/mozilla/cbindgen) - generates C header files from Rust source files. Used in Gecko for WebRender.
  * [safer_ffi](https://github.com/getditto/safer_ffi) - Framework that helps you write foreign function interfaces (FFI) without polluting your Rust code with unsafe { ... } code blocks while making functions far easier to read and maintain.
  * [rusty-cheddar](https://github.com/Sean1708/rusty-cheddar) - generates C header files from Rust source files.
* C++
  * [CXX](https://github.com/dtolnay/cxx) - This library provides a safe mechanism for calling C++ code from Rust and Rust code from C++, not subject to the many ways that things can go wrong when using bindgen or cbindgen to generate unsafe C-style bindings.
  * [rust-bindgen](https://github.com/rust-lang/rust-bindgen) - A Rust bindings generator.
* Erlang
  * [rustler](https://github.com/rusterlium/rustler) - safe Rust bridge for creating Erlang NIF functions.
* Go
  * [rust-plus-golang](https://github.com/mediremi/rust-plus-golang) - This repository shows how, by combining [cgo](https://blog.golang.org/c-go-cgo) and [Rust's FFI capabilities](https://doc.rust-lang.org/book/ffi.html), we can call Rust code from Golang.  
* Haskell
  * [curryrs](https://github.com/mgattozzi/curryrs) - Bridge the gap between Haskell and Rust
  * [haskellrs](https://github.com/mgattozzi/haskellrs) - Rust in Haskell FFI Example
  * [rushs](https://github.com/mgattozzi/rushs) - Haskell in Rust FFI Example
  * [rust-go-example](https://github.com/xyproto/rust-go-example) - An example of how to combine Rust and Go in the same program.
* Java
  * [j4rs](https://crates.io/crates/j4rs) - use Java from Rust.
  * [rjni](https://github.com/benanders/rjni) - use Java from Rust.
  * [java-rust-example](https://github.com/drrb/java-rust-example) - use Rust from Java.
  * [jni](https://crates.io/crates/jni) - use Rust from Java.
  * [jni-sys](https://crates.io/crates/jni-sys) - Rust definitions corresponding to jni.h
  * [rucaja](https://crates.io/crates/rucaja) - use Java from Rust
  * [rust-jdbc](https://github.com/rawrasaur/rust-jdbc) - uses JDBC from Rust.
* Lua
  * [rust-lua53](https://github.com/jcmoyer/rust-lua53) - Lua 5.3 bindings for Rust.
  * [rust-lua](https://github.com/lilyball/rust-lua) - Safe Rust bindings to Lua 5.1
  * [td_rlua](https://github.com/tickbh/td_rlua) - Zero-cost high-level lua 5.3 wrapper for Rust.
  * [hlua](https://github.com/tomaka/hlua) - Rust library to interface with Lua.
* mruby
  * [mrusty](https://github.com/anima-engine/mrusty) - mruby safe bindings for Rust.
* Node.js
  * [neon](https://github.com/neon-bindings/neon) - use Rust from Node.js
  * [node-bindgen](https://github.com/infinyon/node-bindgen) - Easy way to generate nodejs module using Rust.
  * [node-ffi-rs](https://github.com/zhangyuang/node-ffi-rs) - A module written in Rust and N-API provides interface (FFI) features for Node.js.
* Objective-C
  * [rust-objc](https://github.com/SSheldon/rust-objc) - Objective-C Runtime bindings and wrapper for Rust
* Perl
  * [mi-rust](https://github.com/vickenty/mi-rust) - Adds support to M::I for building modules with Cargo
  * [perl-xs](https://github.com/vickenty/perl-xs) - Create Perl XS modules using Rust.
* PHP
  * [phper](https://github.com/phper-framework/phper) - The framework that allows us to write PHP extensions using pure and safe Rust whenever possible.
* Python
  * [milksnake](https://github.com/getsentry/milksnake) - extension for python setuptools that allows you to distribute dynamic linked libraries in Python wheels in the most portable way imaginable.
  * [rust-cpython](https://github.com/dgrunwald/rust-cpython) - Python bindings.
  * [PyO3](https://github.com/PyO3/PyO3) - Rust bindings for the Python interpreter.
  * [RustPython](https://github.com/RustPython/RustPython) — A Python Interpreter written in Rust.
* R
  * [rustr](https://github.com/rustr/rustr) - use Rust from R, and use R in Rust.
* Ruby
  * [ruru](https://github.com/d-unseductable/ruru) - native Ruby extensions written in Rust.
  * [rutie](https://github.com/danielpclark/rutie) - native Ruby extensions written in Rust and vice versa.
  * [helix](https://github.com/tildeio/helix) - write Ruby classes in Rust.
* Web Assembly
  * [wasm-pack](https://github.com/rustwasm/wasm-pack) - pack up the wasm and publish it to npm!
  * [wasm-bindgen](https://github.com/rustwasm/wasm-bindgen) - A project for facilitating high-level interactions between wasm modules and JS.

### Formatters

* [dprint](https://github.com/dprint/dprint) - A pluggable and configurable code formatting platform.
* [Prettier Rust](https://github.com/jinxdash/prettier-plugin-rust) - An opinionated Rust code formatter that autofixes bad syntax ([Prettier](https://prettier.io/) community plugin).
* [rustfmt](https://github.com/rust-lang/rustfmt) -  Rust code formatter maintained by the Rust team and included in cargo.

### IDEs

See also [Are we (I)DE yet?](https://areweideyet.com/) and [Rust Tools](https://www.rust-lang.org/tools).

  * [Atom](https://atom.io/)
    * [atom-language-rust](https://github.com/zargony/atom-language-rust)
  * [Eclipse](https://www.eclipse.org/)
    * [Eclipse Corrosion](https://github.com/eclipse/corrosion)
    * [RustDT](https://github.com/RustDT/RustDT)
  * [Emacs](https://www.gnu.org/software/emacs/)
    * [rust-mode](https://github.com/rust-lang/rust-mode) - Rust Major Mode
    * [flycheck-rust](https://github.com/flycheck/flycheck-rust) - Rust support for [Flycheck](https://github.com/flycheck/flycheck)
    * [emacs-racer](https://github.com/racer-rust/emacs-racer) - Autocompletion (see also [company](https://company-mode.github.io) and [auto-complete](https://github.com/auto-complete/auto-complete))
    * [lsp-rust](https://github.com/emacs-lsp-legacy/lsp-rust) - Add rls support to [lsp-mode](https://github.com/emacs-lsp/lsp-mode)
  * [gitpod.io](https://gitpod.io) - Online IDE with full Rust support based on Rust Language Server
  * [gnome-builder](https://wiki.gnome.org/Apps/Builder) native support for rust and cargo since Version 3.22.2
  * [Kakoune](http://kakoune.org/)
    * [kak-lsp](https://github.com/kakoune-lsp/kakoune-lsp/) - [LSP](https://microsoft.github.io/language-server-protocol/) client. Implemented in Rust and supports rls out of the box.
  * [NetBeans](https://netbeans.org/)
    * [rust-netbeans](https://github.com/drrb/rust-netbeans)
  * [IntelliJ](https://www.jetbrains.com/idea/)
    * [intellij-rust](https://github.com/intellij-rust/intellij-rust)
    * [intellij-toml](https://github.com/intellij-rust/intellij-toml) - basic Toml support
  * [Ride](https://github.com/madeso/ride)
  * [SolidOak](https://github.com/oakes/SolidOak) - A simple IDE for Rust, based on GTK+ and [Neovim](https://github.com/neovim/neovim)
  * [Sublime Text](https://www.sublimetext.com/)
    * [rust-enhanced](https://github.com/rust-lang/rust-enhanced) - official Rust package
    * [packages](https://github.com/sublimehq/Packages/tree/master/Rust) - native Sublime support (already installed)
  * [Vim](https://vim.sourceforge.io/) - the ubiquitous text editor
  * [rust-tools.nvim](https://github.com/simrat39/rust-tools.nvim) - Tools for better development in rust using neovim's builtin lsp.
	* [rust.vim](https://github.com/rust-lang/rust.vim) - provides file detection, syntax highlighting, formatting, Syntastic integration, and more.
	* [vim-cargo](https://github.com/timonv/vim-cargo) - command bindings to quickly run cargo stuff from vim.
	* [vim-racer](https://github.com/racer-rust/vim-racer) - allows vim to use [Racer](https://github.com/racer-rust/racer) for Rust code completion and navigation.
	* [LanguageClient-neovim](https://github.com/autozimu/LanguageClient-neovim) - [LSP](https://microsoft.github.io/language-server-protocol/) client. Implemented in Rust and supports rls out of the box.
  * Visual Studio
    * [VisualRust](https://github.com/PistonDevelopers/VisualRust) - A Visual Studio extension for Rust.
    * [rls-vs2017](https://github.com/dgriffen/rls-vs2017) - Rust support for Visual Studio 2017 Preview.
  * [Visual Studio Code](https://code.visualstudio.com/)
    * [rls-vscode](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust) - Rust support for Visual Studio Code
    * [CodeLLDB](https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb) - A LLDB extension
    * [crates](https://github.com/serayuzgur/crates) - crates is an extension for crates.io dependencies.
    * [vscode-rust](https://marketplace.visualstudio.com/items?itemName=kalitaalexey.vscode-rust) - A fork of RustyCode.

### Pattern recognition

* [rusteval](https://github.com/sfikas/rusteval) - A tool used to evaluate the output of retrieval algorithms.

### Profiling

* [Bencher](https://github.com/bencherdev/bencher) - A suite of continuous benchmarking tools designed to catch performance regressions in CI.
* [criterion.rs](https://github.com/bheisler/criterion.rs) - Statistics-driven benchmarking library for Rust.
* [hyperfine](https://github.com/sharkdp/hyperfine) - A command-line benchmarking tool.
* [hornet](https://github.com/performancecopilot/hornet) - A Performance Co-Pilot memory-mapped values instrumentation library.
* [memory-profiler](https://github.com/nokia/memory-profiler) - A memory profiler for Linux.
* [puffin](https://github.com/EmbarkStudios/puffin) - The friendly little instrumentation profiler for Rust.
* [rust-stopwatch](https://github.com/ellisonch/rust-stopwatch) - A stopwatch library.
* FlameGraphs
  * [torch](https://github.com/mrhooray/torch) - generates FlameGraphs based on DWARF Debug Info
  * [flame](https://github.com/TyOverby/flame)

### Services

* [deps.rs](https://github.com/deps-rs/deps.rs) - Detect outdated or insecure dependencies
* [docs.rs](https://docs.rs) - Automatic documentation generation of crates

### Static analysis

[[assert](https://crates.io/keywords/assert), [static](https://crates.io/keywords/static)]

* [static_assertions](https://crates.io/crates/static_assertions) - Compile-time assertions to ensure that invariants are met.

### Testing

[[testing](https://crates.io/keywords/testing)]

* Code Coverage
  * [tarpaulin](https://crates.io/crates/cargo-tarpaulin) - A code coverage tool designed for Rust.
* Continuous Integration
  * [trust](https://github.com/japaric/trust) - A Travis CI and AppVeyor template to test your Rust crate on 5 architectures and publish binary releases of it for Linux, macOS and Windows
* Frameworks and Runners
  * [cargo-dinghy](https://crates.io/crates/cargo-dinghy/) - A cargo extension to simplify running library tests and benches on smartphones and other small processor devices.
  * [cucumber](https://crates.io/crates/cucumber) - An implementation of the Cucumber testing framework for Rust. Fully native, no external test runners or dependencies.
  * [demonstrate](https://crates.io/crates/demonstrate) - Declarative Testing Framework.
  * [laboratory](https://crates.io/crates/laboratory) - A simple, expressive unit test framework for Rust.
  * [polish](https://github.com/AlKass/polish) - Mini Testing/Test-Driven Framework.  
  * [rewrk](https://github.com/ChillFish8/rewrk) - A more modern http framework benchmarker supporting HTTP/1 and HTTP/2 benchmarks.
  * [rlt](https://github.com/wfxr/rlt) - A universal load testing framework, with real-time tui support.
  * [rstest](https://github.com/la10736/rstest) - Fixture-based test framework for Rust.
  * [ruspec](https://crates.io/crates/ruspec) - Write like Rspec testing framework with rust.  
  * [speculate](https://crates.io/crates/speculate) - An RSpec inspired minimal testing framework for Rust.
  * [test-log](https://github.com/d-e-s-o/test-log) - A replacement of the `#[test]` attribute that initializes logging and/or tracing infrastructure before running tests.
  * [Web Framework Benchmarks](https://github.com/programatik29/rust-web-benchmarks) - Benchmarking web frameworks written in rust with [rewrk](https://github.com/ChillFish8/rewrk) tool.
* Mocking and Test Data
  * [fake-rs](https://github.com/cksac/fake-rs) - A library for generating fake data.
  * [goldenfile](https://github.com/calder/rust-goldenfile) - A library providing a simple API for goldenfile testing.
  * [httpmock](https://github.com/alexliesenfeld/httpmock) - HTTP mocking.
  * [mockiato](https://crates.io/crates/mockiato) - A strict, yet friendly mocking library for Rust 2018.
  * [mockito](https://crates.io/crates/mockito) - HTTP mocking.
  * [faux](https://github.com/nrxus/faux/) - A library to create mocks out of structs.
  * [synth](https://github.com/shuttle-hq/synth/) — Generate database data declaratively.
* Mutation Testing
  * [cargo-mutants](https://github.com/sourcefrog/cargo-mutants) - Finds inadequately tested code by injecting mutations, no source changes required.
  * [mutagen](https://github.com/llogiq/mutagen) - A source-level mutation testing framework (nightly only).
* Property Testing and Fuzzing
  * [afl.rs](https://github.com/rust-fuzz/afl.rs) - A Rust fuzzer, using [AFL](https://lcamtuf.coredump.cx/afl/)  
  * [proptest](https://crates.io/crates/proptest) - property testing framework inspired by the [Hypothesis](https://hypothesis.works/) framework for Python.
  * [quickcheck](https://crates.io/crates/quickcheck) - A Rust implementation of [QuickCheck](https://wiki.haskell.org/Introduction_to_QuickCheck1).


### Transpiling

* [c2rust](https://github.com/immunant/c2rust) - C to Rust translator and cross checker built atop Clang/LLVM.
* [corrode](https://github.com/jameysharp/corrode) - A C to Rust translator written in Haskell.
* [m2cgen](https://github.com/BayesWitnesses/m2cgen) — A CLI tool to transpile trained classic machine learning models into a native Rust code with zero dependencies.

## Libraries

### Artificial Intelligence

#### Genetic algorithms

* [oxigen](https://github.com/Martin1887/oxigen) - Fast, parallel, extensible and adaptable genetic algorithm library. A example using this library solves the N Queens problem for N = 255 in only few seconds and using less than 1 MB of RAM.
* [genevo](https://github.com/innoave/genevo) - Execute genetic algorithm (GA) simulations in a customizable and extensible way.
* [darwin-rs](https://github.com/willi-kappler/darwin-rs) - This library allows you to write evolutionary algorithms (EA) using the Rust programming language. Written by Willi Kappler, License: MIT - Version 0.4 (2017.06.26).
* [RsGenetic](https://github.com/m-decoster/RsGenetic) - Genetic Algorithm library in Rust. In maintenance mode.
* [evo-rs](https://github.com/mneumann/evo-rs) - Evolutionary Algorithm Library for Rust. Without changes for 3 years.
* [revonet](https://github.com/yurytsoy/revonet) - Rust implementation of real-coded GA for solving optimization problems and training of neural networks.

#### Machine learning

[[machine learning](https://crates.io/keywords/machine-learning)]

See also [About Rust’s Machine Learning Community](https://medium.com/@autumn_eng/about-rust-s-machine-learning-community-4cda5ec8a790#.hvkp56j3f).

* [aichat](https://github.com/sigoden/aichat) - Using ChatGPT/GPT-3.5/GPT-4 in the terminal.
* [burn](https://github.com/burn-rs/burn) - A Flexible and Comprehensive Deep Learning Framework in Rust.
* [candle](https://github.com/huggingface/candle) - a minimalist ML framework with a focus on easiness of use and on performance (including GPU support)
* [dfdx](https://github.com/coreylowman/dfdx) - CUDA accelearted machine learning framework that leverages many of Rust's unique features.
* [leaf](https://github.com/autumnai/leaf) - Open Machine Intelligence framework. Abandoned project. The most updated fork is [juice](https://github.com/fff-rs/juice).
* [rusty-machine](https://github.com/AtheMathmo/rusty-machine) - Machine learning library for Rust.
* [RusticSOM](https://github.com/avinashshenoy97/RusticSOM) - Rust library for Self Organising Maps (SOM).
* [smartcore](https://github.com/smartcorelib/smartcore) - Machine Learning Library In Rust.
* [tensorflow/rust](https://github.com/tensorflow/rust) - Rust language bindings for TensorFlow.
* [tokenizers](https://github.com/huggingface/tokenizers) - Hugging Face's tokenizers for modern NLP pipelines written in Rust (original implementation) with bindings for Python.
* [rustlearn](https://github.com/maciejkula/rustlearn) - Machine learning crate for Rust.
* [tch-rs](https://github.com/LaurentMazare/tch-rs) - Rust language bindings for PyTorch.

#### OpenAI

* [async-openai](https://github.com/64bit/async-openai) - Ergonomic Rust bindings for OpenAI API based on OpenAPI spec.
* [tiktoken-rs](https://github.com/zurawiki/tiktoken-rs) - Rust library for tokenizing text with OpenAI models using tiktoken.

### Astronomy

[[astronomy](https://crates.io/keywords/astronomy)]

* [aladin-lite](https://github.com/cds-astro/aladin-lite) - Web application for visualizing spatial and planetary image surveys in different projections.
* [astro-rust](https://github.com/saurvs/astro-rust) - astronomy for Rust.
* [fitsio](https://crates.io/crates/fitsio) - fits interface library wrapping cfitsio.
* [rust-sun](https://github.com/flosse/rust-sun) - A rust port of the JS library suncalc.

### Asynchronous

* [async-backplane](https://github.com/irrustible/async-backplane) - Easy, Erlang-inspired fault-tolerance framework for Rust Futures.
* [async-channel](https://github.com/smol-rs/async-channel) - An async multi-producer multi-consumer channel.
* [async-dropper](https://github.com/t3hmrman/async-dropper) - Implementation of `AsyncDrop`
* [async-std](https://github.com/async-rs/async-std) - Async version of [std](https://doc.rust-lang.org/std/index.html). It provides all the interfaces you are used to, but in an async version and ready for Rust's async/await syntax.
* [mioco](https://github.com/dpc/mioco) - Scalable, coroutine-based, asynchronous IO handling library.
* [fpRust](https://github.com/TeaEntityLab/fpRust) - Monad/MonadIO, Handler, Coroutine/doNotation, Functional Programming features for Rust.
* [futures-lite](https://github.com/smol-rs/futures-lite) - A lightweight async prelude.
* [futures-rs](https://github.com/rust-lang-nursery/futures-rs) - Zero-cost futures in Rust.
* [mio](https://github.com/tokio-rs/mio) - MIO is a lightweight IO library for Rust with a focus on adding as little overhead as possible over the OS abstractions.
* [may](https://github.com/Xudong-Huang/may) - rust stackful coroutine library.
* [runtime](https://github.com/rustasync/runtime) - A runtime agnostic API designed to make async feel like its part of stdlib.

### Audio

[[audio](https://crates.io/keywords/audio)]

* [rust-fmod](https://github.com/GuillaumeGomez/rust-fmod) - [FMOD](https://www.fmod.com) bindings.
* [ears](https://github.com/jhasse/ears) - A simple library to play Sounds and Musics, on top of OpenAL and libsndfile.
* [alto](https://github.com/jpernst/alto) - OpenAL 1.1 bindings.
* [nodi](https://github.com/insomnimus/nodi) - A library for playback and abstraction of MIDI files.
* [portmidi-rs](https://github.com/musitdev/portmidi-rs) - [PortMidi](https://portmedia.sourceforge.net/portmidi/) bindings.
* [hound](https://crates.io/crates/hound) - A WAV encoding and decoding library.
* [rodio](https://github.com/tomaka/rodio) - A Rust audio playback library.
  * [rust-portaudio](https://github.com/RustAudio/rust-portaudio) - [PortAudio](http://www.portaudio.com/) bindings.
* [Symphonia](https://github.com/pdeljanov/Symphonia) - A pure Rust audio decoding and media demuxing library supporting AAC, FLAC, MP3, MP4, OGG, Vorbis, and WAV.

### Authentication

* [canduma](https://github.com/clifinger/canduma) - A Rust authentication server with GraphQL API, Diesel, PostgreSQL session authentication and JWT.
* [jsonwebtoken](https://github.com/Keats/jsonwebtoken) - [JSON Web Token](https://en.wikipedia.org/wiki/JSON_Web_Token) lib in rust.
* [jwtvault](https://github.com/sgrust01/jwtvault) - Async library to manage and orchestrate JWT workflow.
* [oauth2](https://github.com/ramosbugs/oauth2-rs) - Extensible, strongly-typed Rust OAuth2 client library.
* [oxide-auth](https://github.com/HeroicKatora/oxide-auth) - A OAuth2 server library, for use in combination with actix or other frontends, featuring a set of configurable and pluggable backends.
* [totp-rs](https://github.com/constantoine/totp-rs) - 2fa library to generate and verify TOTP-based tokens.
* [yup-oauth2](https://github.com/dermesser/yup-oauth2) - An oauth2 client implementation providing the Device, Installed and Service Account flows.

### Automotive

* [canparse](https://crates.io/crates/canparse) - A CAN signal and definition parser.
* [j2534](https://crates.io/crates/j2534) - J2534 PassThru bindings
* [dlt_parse](https://github.com/JulianSchmid/dlt-parse-rs) - Rust DLT (Diagnostic Log and Trace) packet parser.
* [someip_parse](https://github.com/JulianSchmid/someip-parse-rs) [[someip_parse](https://crates.io/crates/someip_parse)] - A library for parsing the SOME/IP network protocol (without payload interpretation)
* [tuneutils](https://github.com/LibreTuner/tuneutils) [[tuneutils](https://crates.io/crates/tuneutils)] - Utilities for interfacing with, diagnosing, and tuning car
* [can-dbc](https://github.com/marcelbuesing/can-dbc) [[can-dbc](https://crates.io/crates/can-dbc)] - A parser for the DBC format.
* [tokio-socketcan](https://github.com/idletea/tokio-socketcan) - Linux SocketCAN support for tokio based on the socketcan crate.
* [tokio-socketcan-bcm](https://github.com/marcelbuesing/tokio-socketcan-bcm) [[tokio-socketcan-bcm](https://crates.io/crates/tokio-socketcan-bcm)] - Linux SocketCAN BCM support for tokio.
* [socketcan](https://github.com/socketcan-rs/socketcan-rs) [[socketcan](https://crates.io/crates/socketcan)] - Linux SocketCAN library.
* [tokio-socketcan](https://github.com/oefd/tokio-socketcan) [[tokio-socketcan]](https://crates.io/crates/tokio-socketcan)] - Linux SocketCAN support for tokio based on the socketcan crate
* [lin-bus](https://github.com/Sensirion/lin-bus-rs) [[lin-bus](https://crates.io/crates/lin-bus)] - LIN bus driver traits and protocol implementation.

### Bioinformatics

* [Rust-Bio](https://github.com/rust-bio) - bioinformatics libraries in Rust.

### Caching

* [bmemcached-rs](https://github.com/jaysonsantos/bmemcached-rs) - Memcached library written in pure rust.
* [cached](https://github.com/jaemk/cached) - Simple function caching/memoization.
* [cacache-rs](https://github.com/zkat/cacache-rs) -  A high-performance, concurrent, content-addressable disk cache, optimized for async APIs.
* [hitbox](https://github.com/hit-box/hitbox) - Asynchronous caching framework supporting multiple backends.
* [http-cache](https://github.com/06chaynes/http-cache) - A caching middleware that follows HTTP caching rules.
* [moka](https://github.com/moka-rs/moka) - A high performance concurrent caching library for Rust inspired by the Caffeine library for Java.
* [rust-memcache](https://github.com/aisk/rust-memcache) - Memcached client library.
* [sccache](https://github.com/mozilla/sccache/) - Shared Compilation Cache, great for Rust compilation.
* [stretto](https://github.com/al8n/stretto) - A high performance thread-safe memory-bound Rust cache.

### Concurrency

* [archery](https://github.com/orium/archery) [[archery](https://crates.io/crates/archery)] - Library to abstract from `Rc`/`Arc` pointer types.
* [bus-writer](https://github.com/pop-os/bus-writer) - Generic single-reader, multi-writer
* [coroutine-rs](https://github.com/rustcc/coroutine-rs) - Coroutine Library in Rust.
* [coio-rs](https://github.com/zonyitoo/coio-rs) - Coroutine I/O for Rust.
* [crossbeam](https://github.com/crossbeam-rs/crossbeam) - Support for parallelism and low-level concurrency in Rust.
* [glock](https://github.com/aymanmadkour/glock) - Granular locking crate for Rust.
* [kanal](https://github.com/fereidani/kanal) - Kanal is a Rust library to help programmers design effective programs in CSP model via providing featureful multi-producer multi-consumer channels.
* [left-right](https://github.com/jonhoo/left-right) - A lock-free, read-optimized, concurrency primitive.
* [Rayon](https://github.com/rayon-rs/rayon) - A data parallelism library for Rust.
* [scipio](https://github.com/sobolevn/scipio) - Cooperative Thread-per-Core crate for Rust & Linux based on io_uring. Like other rust asynchronous crates it allows one to write asynchronous code that takes advantage of rust async/await, but unlike its counterparts it doesn't use helper threads anywhere.


### Cloud

* AWS [[aws](https://crates.io/keywords/aws)]
  * [rusoto](https://github.com/rusoto/rusoto) - AWS SDK for Rust

* Multi Cloud
  * [Qovery/engine](https://github.com/Qovery/engine) - Abstraction layer library that turns easy application deployment on Cloud providers in just a few minutes.

### Command-line

* Argument parsing
  * [clap-rs](https://github.com/clap-rs/clap) - A simple to use, full featured command-line argument parser.
  * [docopt.rs](https://github.com/docopt/docopt.rs) - A Rust implementation of [DocOpt](http://docopt.org)
  * [pflag](https://github.com/z5labs/pflag) - A port of @spf13's amazing POSIX compliant arg parsing library in Go.
  * [structopt](https://github.com/TeXitoi/structopt) - parse command line argument by defining a struct.
  * [quicli](https://github.com/killercup/quicli) - quickly build cool CLI apps in Rust.
  * [seahorse](https://github.com/ksk001100/seahorse) - A minimal CLI framework written in Rust.
* Data visualization
  * [comfy-table](https://github.com/nukesor/comfy-table) - Beautiful dynamic tables for your cli tools.
  * [rspark](https://github.com/reugn/rspark) - Sparklines for Rust apps.
* Human-centered design.
  * [human-panic](https://github.com/rust-cli/human-panic) - panic messages for humans.
* Line editor
  * [rust-copperline](https://github.com/srijs/rust-copperline) - pure-Rust command line editing library
  * [liner](https://github.com/MovingtoMars/liner) - A library offering readline-like functionality.
  * [linefeed](https://github.com/murarth/linefeed) - Configurable, extensible, interactive line reader.
  * [rustyline](https://github.com/kkawakam/rustyline) - readline implementation in Rust.
* Other
  * [update-informer](https://github.com/mgrachev/update-informer) - Update informer for CLI applications. It checks for a new version on Crates.io and GitHub.
* Pipeline
  * [pager-rs](https://gitlab.com/imp/pager-rs) - pipe your output through an external pager.
  * [rust-subprocess](https://github.com/hniksic/rust-subprocess) - facilities for interaction with external pipelines.
  * [duct.rs](https://github.com/oconnor663/duct.rs) - A builder for subprocess pipelines and IO redirection.
  * [rexpect](https://github.com/rust-cli/rexpect) - automate interactive applications such as ssh, ftp, passwd, etc.
* Progress
  * [indicatif](https://github.com/mitsuhiko/indicatif) - indicate progress to users
  * [pb](https://github.com/a8m/pb) [[pbr](https://crates.io/crates/pbr)] - console progress bar for Rust
  * [spinners](https://github.com/FGRibreau/spinners) - 60+ elegant terminal spinners
* Prompt
  * [terminal_cli.rs](https://github.com/hashmismatch/terminal_cli.rs) - build an interactive command prompt.
* Style
  * [bunt](https://github.com/LukasKalbertodt/bunt) - cross-platform terminal colors and styling with macros.
  * [dialoguer](https://github.com/console-rs/dialoguer) - A rust library for command line prompts and similar things.
  * [rust-ansi-term](https://github.com/ogham/rust-ansi-term) - control colours and formatting on ANSI terminals.
  * [term-painter](https://github.com/LukasKalbertodt/term-painter) - cross-platform styled terminal output.
  * [termstyle](https://github.com/vitiral/termstyle) - build (and test) formatted and styled command line applications
  * [yansi](https://github.com/SergioBenitez/yansi) - A dead simple ANSI terminal color painting library
* TUI
  * [crossterm](https://github.com/TimonPost/crossterm) - crossplatform terminal library
  * [Cursive](https://github.com/gyscos/Cursive) - build rich TUI applications.
  * [inquire](https://github.com/mikaelmello/inquire) - A library for building interactive prompts on terminals.
  * [ratatui](https://github.com/ratatui-org/ratatui) - Rust library to build rich terminal user interfaces (TUIs) and dashboards.
  * [rust-term-grid](https://github.com/ogham/rust-term-grid) - Rust library for putting things in a grid.
  * [ruterm](https://github.com/georgiyozhegov/ruterm) - tiny & simple library for working with TTY.
  * [termion](https://github.com/redox-os/termion) - bindless library for controlling terminals/TTY.
  * [tui-rs](https://github.com/fdehau/tui-rs) - A TUI library inspired by [blessed-contrib](https://github.com/yaronn/blessed-contrib) and [termui](https://github.com/gizak/termui)
  * BearLibTerminal
    * [bearlibterminal](https://github.com/nabijaczleweli/BearLibTerminal.rs) - [BearLibTerminal](https://bitbucket.org/cfyzium/bearlibterminal) bindings.
  * ncurses
    * [ncurses-rs](https://github.com/jeaye/ncurses-rs) - [ncurses](https://www.gnu.org/software/ncurses/) bindings.
    * [pancurses](https://github.com/ihalila/pancurses) - curses library, supports linux and windows.
  * Termbox
    * [rustbox](https://github.com/gchp/rustbox) - bindings to [Termbox](https://github.com/nsf/termbox)

### Compression

* [7z](https://7-zip.org/7z.html)
  * [sevenz-rust](https://github.com/dyz1990/sevenz-rust) - A 7z decompressor/compressor written in pure rust.
* [Brotli](https://opensource.googleblog.com/2015/09/introducing-brotli-new-compression.html)
  * [brotli-rs](https://github.com/ende76/brotli-rs) - implementation of Brotli compression
  * [rust-brotli](https://github.com/dropbox/rust-brotli) - Brotli decompressor in Rust that optionally avoids the stdlib
* bzip2
  * [bzip2-rs](https://github.com/alexcrichton/bzip2-rs) - [libbz2](http://www.bzip.org) bindings
* gzip
  * [zopfli](https://github.com/carols10cents/zopfli) - implementation of the [Zopfli](https://github.com/google/zopfli) compression algorithm for higher quality deflate or zlib compression.
* gzp
  * [gzp](https://github.com/sstadick/gzp) - multi-threaded encoding and decoding of deflate formats and snappy. 
* miniz
  * [flate2-rs](https://github.com/alexcrichton/flate2-rs) - [miniz](https://code.google.com/archive/p/miniz) bindings.
* snappy
  * [rust-snappy](https://github.com/JeffBelgum/rust-snappy) - [snappy](https://github.com/google/snappy) bindings.
* tar
  * [tar-rs](https://github.com/alexcrichton/tar-rs) - tar archive reading/writing in Rust.
* zip
  * [zip2](https://github.com/zip-rs/zip2) - read and write ZIP archives.
* zstd
  * [zstd-rs](https://github.com/gyscos/zstd-rs) - rust binding for the zstd compression library.
* Columnar compression
  * [compressed-vec](https://github.com/velvia/compressed-vec) - SIMD Floating point and integer compressed vector library

### Computation

* [argmin](https://github.com/argmin-rs/argmin) [[argmin](https://crates.io/crates/argmin)] - A pure Rust optimization library.
* [BLAS](https://en.wikipedia.org/wiki/Basic_Linear_Algebra_Subprograms) [[blas](https://crates.io/keywords/blas)]
  * [rust-blas](https://github.com/mikkyang/rust-blas) - BLAS bindings
  * [blas](https://github.com/blas-lapack-rs/blas) - BLAS bindings.
* [Conjugate Gradient](https://en.wikipedia.org/wiki/Limited-memory_BFGS)
  * [cg-sys](https://github.com/noshu/cg-sys) - Rust binding of fortran CG+ subroutine
* [GMP](https://gmplib.org/)
  * [rust-gmp](https://github.com/fizyk20/rust-gmp) - libgmp bindings.
* [GSL](http://www.gnu.org/software/gsl/)
  * [rust-GSL](https://github.com/GuillaumeGomez) - GSL bindings.
* [LAPACK](https://en.wikipedia.org/wiki/LAPACK)
  * [lapack](https://github.com/blas-lapack-rs/lapack) - LAPACK bindings.
* [L-BFGS-B](https://en.wikipedia.org/wiki/Limited-memory_BFGS)
  * [lbfgsb-sys](https://github.com/noshu/lbfgsb-sys) - Rust binding of fortran L-BFGS-B subroutine
* [nalgebra](https://github.com/dimforge/nalgebra) - low-dimensional linear algebra library.
* Parallel
  * [arrayfire-rust](https://github.com/arrayfire/arrayfire-rust) - [Arrayfire](https://github.com/arrayfire) bindings
  * [collenchyma](https://github.com/autumnai/collenchyma) - An extensible, pluggable, backend-agnostic framework for parallel, high-performance computations on CUDA, OpenCL and common host CPU.
  * [rust-opencl](https://github.com/luqmana/rust-opencl) - [OpenCL](https://www.khronos.org/opencl/) bindings.
* Science
  * [russell](https://github.com/cpmech/russell) - Rust Scientific Library for numerical mathematics, ordinary differential equations, special math functions, high-performance (sparse) linear algebra.
  * [scirust](https://github.com/indigits/scirust) - scientific computing library in Rust.
* Statrs
  * [statrs](https://github.com/boxtown/statrs) - Robust statistical computation library in Rust.
* Rustimization [[rustimization](https://crates.io/crates/rustimization)]
  * [rustimization](https://github.com/noshu/rustimization) - A rust optimization library which includes L-BFGS-B and Conjugate Gradient algorithm
* [emu](https://github.com/calebwin/emu) - A language for GPGPU numerical computing from a Rust macro

### Configuration

* [config-rs](https://github.com/mehcode/config-rs) [[config](https://crates.io/crates/config)] - Layered configuration system for Rust applications (with strong support for 12-factor applications).
* [configure_me](https://github.com/Kixunil/configure_me) [[configure_me](https://crates.io/crates/configure_me)] - library for processing application configuration easily.
* [configster](https://github.com/theimpossibleastronaut/configster) [[configster](https://crates.io/crates/configster)] - Rust library for parsing configuration files.
* [justconfig](https://github.com/FlashSystems/just-config) - Easily extendable, layered configuration library that introduces no additional dependencies into your project.
* [uclicious](https://github.com/andoriyu/uclicious) [[uclicious](https://crates.io/crates/uclicious)] - [libUCL](https://github.com/vstakhov/libucl) based feature-rich configuration library.
* [envy](https://github.com/softprops/envy) - deserialize env vars into typesafe structs.

### Cryptography

[[crypto](https://crates.io/keywords/crypto), [cryptography](https://crates.io/keywords/cryptography)]

* [abcrypt](https://github.com/sorairolake/abcrypt) - A simple, modern and secure file encryption library.
* [ark-circom](https://github.com/gakonst/ark-circom) - Arkworks bindings to Circom's R1CS, for Groth16 Proof and Witness generation in Rust.
* [ring](https://github.com/briansmith/ring) - Safe, fast, small crypto using Rust and BoringSSL's cryptography primitives.
* [webpki](https://github.com/briansmith/webpki) - Web PKI TLS X.509 certificate validation in Rust.
* [orion](https://github.com/brycx/orion) - This library aims to provide easy and usable crypto. 'Usable' meaning exposing high-level API's that are easy to use and hard to misuse.
* [rustls](https://github.com/ctz/rustls) - A Rust implementation of TLS
* [rust-crypto](https://github.com/DaGenix/rust-crypto) - cryptographic algorithms in Rust.
* [sodiumoxide](https://github.com/sodiumoxide/sodiumoxide) - [libsodium](https://github.com/jedisct1/libsodium) bindings.
* [libblockchain](https://github.com/doublify/libblockchain) - A Blockchain implementation.
* [exonum](https://github.com/exonum/exonum) - extensible framework for blockchain projects.
* [octavo](https://github.com/libOctavo/octavo) - Modular hash and crypto library in Rust.
* [opaque-ke](https://github.com/novifinancial/opaque-ke) - Pure Rust implementation of the recent [OPAQUE]
* [hashes](https://github.com/RustCrypto/hashes) - Collection of cryptographic hash functions written in pure Rust.
* [mpw-rs](https://github.com/rustindia/mpw-rs) - Pure Rust implementation of the Master Password password manager.
* [rabe](https://github.com/Fraunhofer-AISEC/rabe) - Library providing several Attribute-Based Encryption (ABE) schemes
* [randomorg](https://github.com/vityafx/randomorg) - A random.org client library.
* [rust-djangohashers](https://github.com/racum/rust-djangohashers) - A Rust port of the password primitives used in the Django Project. It doesn't require Django, only hashes and validates passwords according to its style.
* [rncryptor-rs](https://github.com/RNCryptor/rncryptor-rs) - Pure Rust implementation of the RNCryptor AES file format
* [rooster](https://github.com/conradkdotcom/rooster) - Simple password manager to use in your terminal
* [rust-native-tls](https://github.com/sfackler/rust-native-tls) - Bindings for native TLS libraries
* [rust-openssl](https://github.com/sfackler/rust-openssl) - [OpenSSL](https://www.openssl.org/) bindings.
* [rust-security-framework](https://github.com/kornelski/rust-security-framework) - Bindings for Security Framework (OSX native crypto).
* [rustotpony](https://github.com/zebradil/rustotpony) - CLI manager of one-time password generators aka Google Authenticator (TOTP).
* [schannel-rs](https://github.com/steffengy/schannel-rs) - Bindings for Schannel (Windows native TLS).
* [scryptenc-rs](https://github.com/sorairolake/scryptenc-rs) - An implementation of the scrypt encrypted data format.
* [suruga](https://github.com/klutzy/suruga) - A Rust implementation of [TLS 1.2](https://tools.ietf.org/html/rfc5246)
* [themis](https://github.com/cossacklabs/themis) - a high-level cryptographic library for solving typical data security tasks, best fit for multi-platform apps.

* PKCS11 [[pkcs11](https://crates.io/keywords/pkcs11)]
  * [pkcs11](https://github.com/partim/pkcs11) - PKCS11 Rust implementation
  * [pkcs11-rs](https://github.com/myagley/pkcs11-rs) - PKCS11 Rust library
  * [rust-pkcs11](https://github.com/mheese/rust-pkcs11) - Rust PKCS#11 Library

### Database

[[database](https://crates.io/keywords/database)]

* [r2d2](https://github.com/sfackler/r2d2) - generic connection pool.
* NoSQL [[nosql](https://crates.io/keywords/nosql)]

  * [AgateDB](https://github.com/tikv/agatedb) - AgateDB is an embeddable, persistent and fast key-value (KV) database written in pure Rust. It is designed as an experimental engine for the TiKV project, and will bring aggressive optimizations for [TiKV](https://github.com/tikv/tikv) specifically.
  * [ArangoDB](https://www.arangodb.com)
     * [Rincon](https://github.com/innoave/rincon) - An ArangoDB (NoSQL and Graph store) driver for Rust
  * [Cassandra](http://cassandra.apache.org) [[cassandra](https://crates.io/keywords/cassandra), [cql](https://crates.io/keywords/cql)]
    * [cdrs](https://github.com/AlexPikalov/cdrs) - native client written in Rust.
    * [cdrs-tokio](https://github.com/krojew/cdrs-tokio) - Cassandra protocol implementation in Rust, production-ready async Apache Cassandra driver written in pure Rust.
    * [cassandra-rs](https://github.com/cassandra-rs/cassandra-rs) -  bindings to the DataStax C/C++ client.
  * CouchDB [[couchdb](https://crates.io/keywords/couchdb)]
    * [chill](https://github.com/chill-rs/chill) - A Rust client for the CouchDB REST API.
    * [Sofa](https://github.com/66Origin/sofa) - an interface to CouchDB HTTP REST API for stable rust.
  * Crux [[crux](https://crates.io/keywords/crux)]
    * [transistor](https://github.com/naomijub/transistor) - A Crux Database Client.  
  * [DynamoDB](https://aws.amazon.com/dynamodb/) [[dynamodb](https://crates.io/keywords/dynamodb)]
    * [dynomite](https://github.com/softprops/dynomite) - A library for strongly-typed and convenient interaction with `rusoto_dynamodb`.
  * Elasticsearch [[elasticsearch](https://crates.io/keywords/elasticsearch)]
    * [rs-es](https://github.com/benashford/rs-es) - A Rust client for the [Elastic](https://www.elastic.co/) REST API.
    * [elastic-reqwest](https://github.com/elastic-rs/elastic-reqwest) - A lightweight implementation of the Elasticsearch API based on Reqwest.
    * [elastic](https://github.com/elastic-rs/elastic) - elastic is an efficient, modular API client for Elasticsearch written in Rust
    * [ZincObserve](https://github.com/zinclabs/zincobserve) - 10x easier, 10x cheaper, high performance, petabyte scale - Elasticsearch/Splunk/Datadog alternative for rocket (logs, metrics, traces).
  * etcd
    * [rust-etcd](https://github.com/jimmycuadra/rust-etcd) - A client library for CoreOS's etcd.
  * ForestDB
    * [sherwood](https://github.com/vhbit/sherwood) - [ForestDB](https://github.com/couchbase/forestdb) bindings.
  * [InfluxDB](https://www.influxdata.com/)
    * [influxdb-rs](https://github.com/panoptix-za/influxdb-rs) - asynchronous interface.
    * [InfluxDBClient-rs](https://github.com/driftluo/InfluxDBClient-rs) - Synchronization interface.
  * [IndraDB](https://github.com/indradb/indradb) - A graph database written in rust.
  * LevelDB
    * [leveldb](https://github.com/skade/leveldb) - [LevelDB](https://github.com/google/leveldb) bindings.
  * LMDB [[lmdb](https://crates.io/keywords/lmdb)]
    * [lmdb-rs](https://github.com/vhbit/lmdb-rs) - [LMDB](https://symas.com/lmdb/) bindings.
  * MongoDB [[mongodb](https://crates.io/keywords/mongodb)]
    * [mongo-rust-driver-prototype](https://github.com/mongodb-labs/mongo-rust-driver-prototype) - [MongoDB](https://www.mongodb.com/) bindings.
  * Neo4j [[cypher](https://crates.io/keywords/cypher), [neo4j](https://crates.io/keywords/neo4j)]
  * [PoloDB](https://github.com/vincentdchan/PoloDB) - An embedded JSON-based database has API similar to MongoDB. [PoloDB](https://www.polodb.org/)
  * [Redb](https://www.redb.org/)
    * [Redb](https://github.com/cberner/redb) - An embedded key-value database written in pure Rust. It provides a similar interface to other embedded key-value stores such as rocksdb and lmdb.
  * Redis [[redis](https://crates.io/keywords/redis)]
    * [redis-protocol](https://github.com/aembke/redis-protocol.rs) - Structs and functions for implementing the Redis protocol.
    * [redis-rs](https://github.com/mitsuhiko/redis-rs) - [Redis](https://redis.io/) library in Rust.
    * [RedisJSON](https://github.com/RedisJSON/RedisJSON) - RedisJSON - a JSON data type for Redis.
    * [rrtop](https://github.com/wojciech-zurek/rrtop) - Redis monitoring (top like) app. rrtop -> [r]ust [r]edis [top]
  * [RocksDB](https://rocksdb.org/)
    * [rust-rocksdb](https://github.com/rust-rocksdb/rust-rocksdb) - RocksDB bindings.
  * [UnQLite](https://github.com/symisc/unqlite)
    * [unqlite.rs](https://github.com/zitsen/unqlite.rs) - UnQLite bindings.
  * [ZooKeeper](https://zookeeper.apache.org/)
    * [rust-zookeeper](https://github.com/bonifaido/rust-zookeeper) - A client library for Apache ZooKeeper.
  * [PickleDB](https://pythonhosted.org/pickleDB/)
    * [pickledb-rs](https://github.com/seladb/pickledb-rs) - a lightweight and simple key-value store, heavily inspired by Python's PickleDB.
* SQL [[sql](https://crates.io/keywords/sql)]
  * Microsoft SQL
    * [tiberius](https://github.com/steffengy/tiberius)
  * MySql [[mysql](https://crates.io/keywords/mysql)]
    * [mysql-proxy-rs](https://github.com/AgilData/mysql-proxy-rs) - A MySQL Proxy.
    * [mysql_async](https://github.com/blackbeam/mysql_async) - asyncronous Rust Mysql driver based on Tokio.
    * [rust-mysql-simple](https://github.com/blackbeam/rust-mysql-simple) - A native MySql client.
  * Oracle [[oracle](https://crates.io/keywords/oracle)]
    * [rust-oracle](https://github.com/kubo/rust-oracle) - Oracle driver for Rust.
    * [mimir](https://github.com/aesir-vanir/mimir) - Rust FFI bindings over ODPI-C.
  * PostgreSql [[postgres](https://crates.io/keywords/postgres), [postgresql](https://crates.io/keywords/postgresql)]
    * [rust-postgres](https://github.com/sfackler/rust-postgres) - A native [PostgreSQL](https://www.postgresql.org/) client.
  * Sqlite [[sqlite](https://crates.io/keywords/sqlite)]
    * [rusqlite](https://github.com/jgallagher/rusqlite) - [Sqlite3](https://www.sqlite.org/index.html) bindings.
* ORM [[orm](https://crates.io/keywords/orm)]
  * [diesel](https://github.com/diesel-rs/diesel) - an ORM and Query builder for Rust.
  * [njord](https://github.com/njord-rs/njord) - A lightweight ORM library for Rust.
  * [rbatis](https://github.com/rbatis/rbatis) - Rust ORM Framework High Performance(JSON based).
  * [rustorm](https://github.com/ivanceras/rustorm) - an ORM for Rust.
  * [sea-orm](https://github.com/SeaQL/sea-orm) - An async & dynamic ORM for Rust.
  * [seaography](https://github.com/SeaQL/seaography) - GraphQL framework for SeaORM.

### Data processing

* [ndarray](https://github.com/rust-ndarray/ndarray) - N-dimensional array with array views, multidimensional slicing, and efficient operations.
* [pg_analytics](https://github.com/paradedb/paradedb/tree/dev/pg_analytics) - PostgreSQL extension that accelerates analytical query processing inside Postgres to a performance level comparable to dedicated OLAP databases.
* [pg_lakehouse](https://github.com/paradedb/paradedb/tree/dev/pg_lakehouse) - PostgreSQL extension that transforms Postgres into an analytical query engine over object stores like AWS S3/GCS and table formats like Delta Lake/Iceberg.
* [polars](https://github.com/pola-rs/polars) - Polars is a blazingly fast DataFrames library implemented in Rust using [Apache Arrow Columnar Format](https://arrow.apache.org/docs/format/Columnar.html) as the memory model.
* [utah](https://github.com/kernelmachine/utah) - Dataframe structure and operations in Rust.
* [weld](https://github.com/weld-project/weld) - High-performance runtime for data analytics applications.
* [yata](https://github.com/amv-dev/yata) - high perfomance technical analysis library.

### Data streaming

* [arroyo](https://github.com/ArroyoSystems/arroyo) - High-performance real-time analytics in Rust and SQL.
* [iggy](https://github.com/iggy-rs/iggy) - Persistent message streaming platform, supporting QUIC, TCP and HTTP transport protocols.
* [fluvio](https://github.com/infinyon/fluvio) - Programmable high-performance distributed data streaming platform, built to make it easy to run real-time applications.

### Data structures

* [Algorithms](https://github.com/TheAlgorithms/Rust) - All Algorithms implemented in Rust.
* [array_tool](https://github.com/danielpclark/array_tool) - Array helpers for Rust. Some of the most common methods you would use on Arrays made available on Vectors. Polymorphic implementations for handling most of your use cases.
* [contain-rs](https://github.com/contain-rs) - Extension of Rust's std::collections
* [enum-map](https://codeberg.org/xfix/enum-map) - An optimized map implementation for enums using an array to store values.
* [extprim](https://github.com/kennytm/extprim) - Extra primitive types (u128, i128) for Rust.
* [flatbuffers-pool](https://github.com/keithnoguchi/flatbuffers-pool) - RAII based FlatBufferBuilder pool.
* [generic-array](https://github.com/fizyk20/generic-array) - a hack to allow for arrays sized by typenums.
* [grid](https://github.com/becheran/grid) - Provide a two dimensional data structure for rust that is easy to use and fast.
* [hypergraph](https://github.com/yamafaktory/hypergraph) - Hypergraph is a data structure library to generate directed hypergraphs.
* [kdtree-rs](https://github.com/mrhooray/kdtree-rs) - K-dimensional tree in Rust for fast geospatial indexing and nearest neighbors lookup.
* [nutype](https://github.com/greyblake/nutype) - define newtype structures with validation constraints.
* [priority-queue](https://github.com/garro95/priority-queue)[[priority-queue](https://crates.io/crates/priority-queue)] - A priority queue that implements priority changes.
* [progress-streams](https://github.com/pop-os/progress-streams) - Progress callbacks for types which implement `dyn io::Read` or `dyn io::Write`.
* [roaring-rs](https://github.com/RoaringBitmap/roaring-rs) - Roaring Bitmaps in Rust.
* [rpds](https://github.com/orium/rpds) [[rpds](https://crates.io/crates/rpds)] - Persistent data structures in Rust.
* [rust-itertools](https://github.com/rust-itertools/itertools)
* [scapegoat](https://github.com/tnballo/scapegoat) - Safe, fallible, stack-only alternative to BTreeSet and BTreeMap.
* [simsimd](https://github.com/ashvardanian/SimSIMD) - SIMD-accelerated vector distances and similarity functions for x86 AVX2 & AVX-512, and Arm NEON.
* [tst](https://github.com/billyevans/tst) [[tst](https://crates.io/crates/tst)] - Ternary search tree collection.
* [usher](https://github.com/whitfin/usher) [[usher](https://crates.io/crates/usher)] - Parameterized routing for generic resources in Rust.

### Data visualization

* [egui_graphs](https://github.com/blitzarx1/egui_graphs) - Interactive graph visualization widget for rust powered by egui and petgraph.
* [gust](https://github.com/saresend/Gust) - A charting library for rust.
* [pgfplots](https://github.com/djduque/pgfplots) - A Rust library to generate publication-quality figures.
* [plotly](https://github.com/igiagkiozis/plotly) - Plotly for Rust.
* [plotlib](https://github.com/milliams/plotlib)
* [plotpy](https://github.com/cpmech/plotpy) - Rust plotting library using Python (Matplotlib).
* [plotters](https://github.com/plotters-rs/plotters)
* [pretty-hex](https://github.com/wolandr/pretty-hex) - A Rust library providing pretty hex dump.
* [rerun](https://github.com/rerun-io/rerun) - An SDK for logging computer vision and robotics data (tensors, point clouds, etc) paired with a visualizer for exploring that data over time.

### Date and time

[[date](https://crates.io/keywords/date), [time](https://crates.io/keywords/time)]

* [chrono](https://github.com/chronotope/chrono) - Date and time library for Rust.
* [ms](https://github.com/Mnwa/ms) - it's a library for converting human-like times to milliseconds.
* [nt-time](https://github.com/sorairolake/nt-time) - A Windows file time library. 
* [rust-persian-calendar](https://github.com/yaa110/rust-persian-calendar)
* [time](https://github.com/time-rs/time)

### Distributed systems

* [Astria](https://github.com/astriaorg/astria) - A monorepo containing all the custom components of the Astria network, a decentralized system that replaces traditional sequencers, offering a shared, permissionless sequencer network.
* [Aurae](https://github.com/aurae-runtime/aurae) - Distributed systems runtime daemon written in Rust.
* [Ballista](https://github.com/ballista-compute/ballista) - Ballista is a distributed compute platform primarily implemented in Rust, using Apache Arrow as the memory model. It is built on an architecture that allows other programming languages to be supported as first-class citizens without paying a penalty for serialization costs.
* [Bastion](https://github.com/bastion-rs/bastion) -  Highly-available, fault-tolerant runtime system with dynamic, dispatch-oriented, lightweight process model. It supplies actor-model-like concurrency with a lightweight process implementation and utilizes all of the system resources efficiently guaranteeing of at-most-once message delivery.
* [Coerce-rs](https://github.com/LeonHartley/Coerce-rs) - Coerce-rs is an asynchronous (async/await) Actor runtime and distributed system framework for Rust. It allows for extremely simple yet powerful actor-based distributed system development.
* [Malachite](https://github.com/informalsystems/malachite) - Malachite is a Byzantine-fault tolerant (BFT) consensus engine implemented in Rust.
* Antimony
  * [antimony](https://github.com/antimonyproject/antimony) [[antimony](https://crates.io/crates/antimony)] - stream processing / distributed computation platform.
* Apache Hadoop
  * [efflux](https://github.com/whitfin/efflux) - Easy Hadoop Streaming and MapReduce interfaces in Rust.
* Apache Kafka
  * [rust-rdkafka](https://github.com/fede1024/rust-rdkafka) [[rdkafka](https://crates.io/crates/rdkafka)] - [librdkafka](https://github.com/edenhill/librdkafka) bindings.
  * [kafka-rust](https://github.com/spicavigo/kafka-rust)
* Beanstalkd
  * [rust-beanstalkd](https://github.com/schickling/rust-beanstalkd) - [Beanstalkd](https://github.com/beanstalkd/beanstalkd) bindings.
* HDFS
  * [hdfs-rs](https://github.com/hyunsik/hdfs-rs) - libhdfs bindings.
* Other
  * [ockam](https://github.com/build-trust/ockam) - End-to-End Encryption, Mutual Authentication, and ABAC for distributed applications.

### Domain driven design

* [cqrs](https://github.com/serverlesstechnology/cqrs) - A framework for CQRS and event sourcing with [user guide](https://doc.rust-cqrs.org/).

### Email

[[email](https://crates.io/keywords/email), [imap](https://crates.io/keywords/imap), [smtp](https://crates.io/keywords/smtp)]

* [Gatarashii_imap](https://github.com/GildedHonour/atarashii_imap) - 新しい (atarashii/new) IMAP client in Rust. It supports plain and secure connections.
* [imap-codec](https://github.com/duesee/imap-codec) - Rock-solid and complete codec for IMAP.
* [sendgrid-rs](https://github.com/gsquire/sendgrid-rs) - unofficial Rust library for SendGrid API.
* [lettre](https://github.com/lettre/lettre) - an SMTP-library for Rust.
* [mail-auth](https://github.com/stalwartlabs/mail-auth) - DKIM, ARC, SPF and DMARC message authentication library.
* [mail-parser](https://github.com/stalwartlabs/mail-parser) - A fast and robust e-mail parsing library with full MIME support.
* [mailcrab](https://github.com/tweedegolf/mailcrab) - Email test server for development.
* [mailparse](https://github.com/staktrace/mailparse) - A library for parsing real-world email files.
* [mailtutan](https://github.com/mailtutan/mailtutan) - An SMTP server for test and development environment.
* [meli](https://github.com/meli/meli) - terminal mail client.
* [mrml](https://github.com/jdrouet/mrml) - A library to generate nice email templates working on any mail client.
* [catapulte](https://github.com/jdrouet/catapulte) - A microservice to send emails using [MRML](https://github.com/jdrouet/mrml) templates.
* [jolimail](https://github.com/jdrouet/jolimail) - A web application to build [MRML](https://github.com/jdrouet/mrml) templates.
* [vSMTP](https://github.com/viridIT/vSMTP) - A next-gen Mail Transfer Agent (MTA) written in Rust.

### Encoding

[[encoding](https://crates.io/keywords/encoding)]

* ASN.1
  * [rust-asn1](https://github.com/alex/rust-asn1) - A Rust ASN.1 (DER) serializer.
* Bencode
  * [rust-bencode](https://github.com/arjantop/rust-bencode) - [Bencode](https://en.wikipedia.org/wiki/Bencode) implementation in Rust.
* Binary
  * [nue](https://github.com/arcnmx/nue) - I/O and binary data encoding for Rust.
  * [bincode](https://github.com/TyOverby/bincode) - A binary encoder/decoder in Rust.
  * [goblin](https://github.com/m4b/goblin) [[goblin](https://crates.io/crates/goblin)] -  cross-platform, zero-copy, and endian-aware binary parsing.
* BSON
  * [bson-rs](https://github.com/zonyitoo/bson-rs)
* Byte swapping
  * [byteorder](https://github.com/BurntSushi/byteorder) - Supports big-endian, little-endian and native byte orders.
* Cap'n Proto
  * [capnproto-rust](https://github.com/capnproto/capnproto-rust)
* CBOR
  * [serde_cbor](https://crates.io/crates/serde_cbor) - CBOR support for serde.
* Character Encoding
  * [encoding_rs](https://github.com/hsivonen/encoding_rs) [[encoding_rs](https://crates.io/crates/encoding_rs)] - A Gecko-oriented implementation of the Encoding Standard in Rust.
  * [rust-encoding](https://github.com/lifthrasiir/rust-encoding)
* CRC
  * [crc-rs](https://github.com/mrhooray/crc-rs)
* CSV
  * [rust-csv](https://github.com/BurntSushi/rust-csv) - A fast and flexible CSV reader and writer, with support for Serde.
* [FlatBuffers](https://google.github.io/flatbuffers/)
  * [flatc-rust](https://github.com/frol/flatc-rust) - FlatBuffers compiler (flatc) integration for Cargo build scripts.
* EDN
  * [edn-rs](https://github.com/naomijub/edn-rs) - crate to parse and emit EDN format into Rust types.  
* HAR
  * [har-rs](https://github.com/mandrean/har-rs) - A HTTP Archive Format (HAR) serialization & deserialization library.
* HTML
  * [html5ever](https://github.com/servo/html5ever) - High-performance browser-grade HTML5 parser.
  * [rust-htmlescape](https://github.com/veddan/rust-htmlescape) - encoding/decoding HTML entities.
* JSON
  * [gjson.rs](https://github.com/tidwall/gjson.rs) - Get JSON values quickly - JSON parser for Rust.
  * [json](https://github.com/serde-rs/json) [[serde\_json](https://crates.io/crates/serde_json)] - JSON support for [Serde](https://github.com/serde-rs/serde) framework.
  * [json-rust](https://github.com/maciejhirsz/json-rust) [[json](https://crates.io/crates/json)] - JSON implementation in Rust.
  * [pikkr](https://github.com/pikkr/pikkr) [[pikkr](https://crates.io/crates/pikkr)] - JSON parser which picks up values directly without performing tokenization in Rust.
* Jsonnet
  * [rust-jsonnet](https://github.com/Qihoo360/rust-jsonnet) - The Google Jsonnet( operation data template language) for rust.
* MsgPack
  * [msgpack-rust](https://github.com/3Hren/msgpack-rust) - A pure Rust low/high level MessagePack implementation.
* PEM
  * [pem-rs](https://github.com/jcreekmore/pem-rs) [[pem](https://crates.io/crates/pem)] - A Rust based way to parse and encode PEM-encoded data.
* Postman Collection
  * [postman-collection-rs](https://github.com/mandrean/postman-collection-rs) - A Postman Collection v1, v2 & v2.1 serialization & deserialization library.
* ProtocolBuffers
  * [prost](https://github.com/danburkert/prost) - Protocol Buffers implementation for the Rust Language.
  * [rust-protobuf](https://github.com/stepancheg/rust-protobuf)
* RON (Rusty Object Notation)
  * [https://github.com/ron-rs/ron](https://github.com/ron-rs/ron)
* Tnetstring
  * [rust-tnetstring](https://github.com/erickt/rust-tnetstring)
* TOML
  * [taplo](https://github.com/tamasfe/taplo) - A TOML toolkit written in Rust.
  * [toml-rs](https://github.com/toml-rs/toml)
* XML
  * [quick-xml](https://github.com/tafia/quick-xml) - High performance XML pull reader/writer.
  * [RustyXML](https://github.com/Florob/RustyXML) - an XML parser written in Rust.
  * [sxd-document](https://github.com/shepmaster/sxd-document) - An XML library in Rust.
  * [sxd-xpath](https://github.com/shepmaster/sxd-xpath) - An XPath library in Rust.
  * [xml-rs](https://github.com/netvl/xml-rs) - A streaming XML library.
  * [yaserde](https://github.com/media-io/yaserde) - Yet Another Serializer/Deserializer specialized for XML.
* YAML
  * [yaml-rust](https://github.com/chyh1990/yaml-rust) - The missing YAML 1.2 implementation for Rust.
  * [serde-yaml](https://github.com/dtolnay/serde-yaml) [[serde\_yaml](https://crates.io/crates/serde_yaml)] - YAML support for [Serde](https://github.com/serde-rs/serde) framework.
  * [libyaml-rust](https://github.com/kimhyunkang/libyaml-rust) - [libyaml](https://pyyaml.org/wiki/LibYAML) bindings.
  * [stfu8](https://github.com/vitiral/stfu8) - Sorta Text Format in UTF-8.

### Filesystem

[[filesystem](https://crates.io/keywords/filesystem)]

* Libraries
  * [rust-file-seq](https://github.com/jonhkr/rust-file-seq) - Fail-safe sequence implementation that uses the file system as store.

* Operations
  * [Camino](https://github.com/camino-rs/camino) - Like Rust's std::path::Path, but UTF-8.
  * [Rust_Search](https://github.com/ParthJadhav/Rust_Search) - Blazingly fast file search library built in Rust.
  * [dbus-udisks2](https://github.com/pop-os/dbus-udisks2) -> UDisks2 DBus API
  * [sys-mount](https://github.com/pop-os/sys-mount) - High level abstraction for the `mount` / `umount2` system calls.
  * [path_abs](https://github.com/vitiral/path_abs) - Absolute serializable path types and associated methods.
  * [fs_extra](https://github.com/webdesus/fs_extra) - expanding opportunities standard library std::fs and std::io
* Temporary Files
  * [tempdir](https://github.com/rust-lang-deprecated/tempdir) - temporary directory library.
  * [tempfile](https://github.com/Stebalien/tempfile) - temporary file library.
  * [xattr](https://github.com/Stebalien/xattr) [[xattr](https://crates.io/crates/xattr)] - list and manipulate unix extended file attributes.
  * [zbox](https://github.com/zboxfs/zbox) [[zbox](https://crates.io/crates/zbox)] - Zero-details, privacy-focused embeddable file system.

### Financial

* [apca](https://github.com/d-e-s-o/apca) - Opinionated and comprehensive bindings to the [Alpaca API](https://alpaca.markets/) for stock trading and more.
* [bank-rs](https://gitlab.com/rubdos/bank-rs) - Provides interfaces for bank transactions.
* [currency-rs](https://github.com/Tahler/currency-rs) - A very small library, providing a way to represent currencies in Rust.
* [iso20022](https://github.com/keithnoguchi/iso20022-rs) - [ISO 20022](https://www.iso20022.org/) Universal Financial Industry Messages in Rust.
* [iso_currency](https://github.com/zbrox/iso_currency) - This crate provides an enum that represents all ISO 4217 currencies and has simple methods to convert between numeric and character code, list of territories where each currency is used, the symbol, and the English name of the currency.
* [iso-4217](https://github.com/mechiru/iso-4217) - This library provides enumeration of [ISO-4217](https://en.wikipedia.org/wiki/ISO_4217).
* [diem](https://github.com/diem/diem) - Libra Core implements a decentralized, programmable database which provides a financial infrastructure that can empower billions of people.
* [hyperswitch](https://github.com/juspay/hyperswitch) - An open source payments orchestrator that lets you connect with multiple payment processors and route payment traffic effortlessly, all with a single API integration.
* [offset](https://github.com/freedomlayer/offset) - Offset is a credit card powered by trust between people.
* [OpenLibra CLI](https://github.com/open-libra/cli) - The "open-libra" Command Line Interface (CLI) utility for devnet setup.
* [penny](https://github.com/bb010g/penny) - ISO-4217–based currency library for Rust.
* [rust-decimal](https://github.com/BurntSushi/rust-decimal) - A Decimal Implementation written in pure Rust suitable for financial calculations.
* [rusty-money](https://github.com/varunsrin/rusty_money) - A library that handles calculating, rounding, displaying, and parsing units of money according to ISO 4217 standards. The main items exported by the library are Money and Currency.
* [RustQuant](https://github.com/avhz/RustQuant) - A quantitative finance library.

#### ISO8583

[[ISO8583](https://crates.io/keywords/ISO8583)]

* [iso8583](https://github.com/rohitjoshi/iso8583) - Iso8583 Message Parser (Serialize/Deserialize).
* [iso8583_rs](https://github.com/rkbalgi/iso8583_rs) - ISO8583 library written in Rust.
* [lakgves](https://github.com/timgabets/lakgves) - HTTP to ISO8583 message converter, written from scratch in Rust and heavily inspired by [JUKS' socket queue](https://github.com/juks/iso-8583-socket-queue). The main purpose of the system is testing Issuer Bank systems.

### Framework

* [Abscissa](https://github.com/iqlusioninc/abscissa) - Microframework for building Rust applications (either CLI tools or network/web services), aiming to provide a large number of features with a minimal number of dependencies, and with a strong focus on security.
* [exonum](https://github.com/exonum/exonum) - Extensible open-source framework for creating blockchain applications. Exonum can be used to create cryptographically powered distributed ledgers in virtually any problem domain, including FinTech, GovTech, and LegalTech. The Exonum framework is oriented towards creating permissioned blockchains, that is, blockchains with the known set of blockchain infrastructure providers.
* [ntex](https://github.com/ntex-rs/ntex) - Framework for composable network services.
* [salvo](https://github.com/salvo-rs/salvo) - Salvo is a simple web framework written by rust. It is simple to use it to build website, REST API.
* [Seed](https://github.com/seed-rs/seed) - Seed is a front-end Rust framework for creating fast and reliable web apps with an elm-like architecture.
* [Shio](https://github.com/mehcode/shio-rs) - Shio is a fast, simple, and asynchronous micro web-framework for Rust.
* [tsukuyomi](https://github.com/tsukuyomi-rs/tsukuyomi) - Asynchronous Web framework for Rust.

### Game development

See also [Are we game yet?](http://arewegameyet.com)
* Allegro
  * [RustAllegro](https://github.com/SiegeLord/RustAllegro) - [Allegro 5](https://liballeg.org/) bindings.
* Bracket-lib (previously RLTK)
  * [bracket-lib](https://github.com/amethyst/bracket-lib) - The Roguelike Toolkit (RLTK), implemented for Rust.
* Challonge
  * [challonge-rs](https://github.com/vityafx/challonge-rs) [[challonge](https://crates.io/crates/challonge)] - Client library for the Challonge REST API. Helps to organize tournaments.
* Corange
  * [corange-rs](https://github.com/lucidscape/corange-rs) - [Corange](https://github.com/orangeduck/Corange) bindings
* Entity-Component Systems (ECS)
  * [specs](https://github.com/slide-rs/specs) - Specs Parallel ECS.
* Game Engines
  * [Amethyst](https://amethyst.rs) - Data-oriented game engine.
  * [Bevy](https://github.com/bevyengine/bevy) - is a refreshingly simple data-driven game engine built in Rust.
  * [Piston](https://www.piston.rs/)
  * [ggez](https://github.com/ggez/ggez) - A lightweight game framework for making 2D games with minimum friction.
  * [Kiss3D](http://kiss3d.org/) - A Keep It Simple, Stupid 3d graphics engine written with Rust
  * [Unrust](https://github.com/unrust/unrust) - unrust - A pure rust based (webgl 2.0 / native) game engine
  * [Vulkust](https://github.com/Hossein-Noroozpour/vulkust) - Vulkust - A safe, highly multithreaded, Vulkan based game engine written with Rust.
* [Godot](https://godotengine.org/)
  * [godot-rust](https://github.com/godot-rust/godot-rust) [[gdnative](https://crates.io/crates/gdnative)] - Rust bindings to the Godot game engine.
* [SDL](http://www.libsdl.org/) [[sdl](https://crates.io/keywords/sdl)]
  * [rust-sdl](https://github.com/brson/rust-sdl) - SDL1 bindings.
  * [rust-sdl2](https://github.com/Rust-SDL2/rust-sdl2) - SDL2 bindings.
* SFML
  * [rust-sfml](https://github.com/jeremyletang/rust-sfml) - [SFML](https://www.sfml-dev.org/) bindings.
* Tcod-rs
  * [tcod-rs](https://github.com/tomassedovic/tcod-rs) - Libtcod bindings for Rust.
* Victorem
  * [Victorem](https://github.com/VictoremWinbringer/Victorem) [[Victorem](https://crates.io/crates/Victorem)] - Easy UDP Game Server and UDP Client framework for creating simple 2D and 3D online game prototype.
* Voxlap
  * [rust-voxlap](https://github.com/bbodi/rust-voxlap) - [Voxlap](http://advsys.net/ken/voxlap.htm) bindings

### Geospatial

[[geo](https://crates.io/keywords/geo), [gis](https://crates.io/keywords/gis)]

* [coord_transforms](https://github.com/DaveKram/coord_transforms) [[coord_transforms](https://crates.io/crates/coord_transforms)] - coordinate transformations (2-d, 3-d, and geospatial)
* [Georust](https://github.com/georust) - geospatial tools and libraries written in Rust.
* [Martin](https://github.com/maplibre/martin) - Map tile server with PostGIS, MBTiles, PMTiles, and sprites support.
* [rust-reverse-geocoder](https://github.com/llambda/rrgeo) - A fast, offline reverse geocoder in Rust, inspired by https://github.com/thampiman/reverse-geocoder
* [geomorph](https://github.com/vlopes11/geomorph) [[geomorph](https://crates.io/crates/geomorph)] - conversion between UTM, LatLon and MGRS coordinates.

### Graph algorithms

* [graph](https://github.com/neo4j-labs/graph) - A library for high-performant graph algorithms.
* [petgraph](https://github.com/petgraph/petgraph) - Graph data structure library for Rust.

### Graphics

[[graphics](https://crates.io/keywords/graphics)]

* [gfx](https://github.com/gfx-rs/gfx) - A high-performance, bindless graphics API for Rust.
* Font
  * [rusttype](https://github.com/redox-os/rusttype) - A pure Rust alternative to libraries like FreeType.
* OpenGL [[opengl](https://crates.io/keywords/opengl)]
  * [gl-rs](https://github.com/brendanzab/gl-rs)
  * [glium](https://github.com/glium/glium) - safe OpenGL wrapper for the Rust language.
  * [Kiss3d](http://kiss3d.org) - draw simple geometric figures and play with them with one-liners.
  * [glfw-rs](https://github.com/PistonDevelopers/glfw-rs)
  * [glutin](https://crates.io/crates/glutin) - Rust alternative to [GLFW](https://www.glfw.org/)
* PDF
  * [libharu_ng](https://github.com/bastibense/libharu_ng) - Easily generate PDFs from your Rust app.
  * [rust-pdf](https://github.com/kaj/rust-pdf)
  * [printpdf](https://github.com/fschutt/printpdf) - PDF writing library.
  * [lopdf](https://github.com/J-F-Liu/lopdf) - PDF document manipulation.
* [Vulkan](https://www.khronos.org/vulkan/) [[vulkan](https://crates.io/keywords/vulkan)]
  * [vulkano](https://github.com/vulkano-rs/vulkano) [[vulkano](https://crates.io/crates/vulkano)]

### Graph processing

* [tinkerpop-rs](https://github.com/kud1ing/tinkerpop-rs) - an example how to use Apache TinkerPop from Rust.

### GUI

[[gui](https://crates.io/keywords/gui)]

* [autopilot-rs](https://github.com/autopilot-rs/autopilot-rs) - A simple, cross-platform GUI automation library for Rust.
* [azul](https://github.com/fschutt/azul) - A free, functional, IMGUI-oriented GUI framework for rapid development of desktop applications written in Rust, supported by the Mozilla WebRender rendering engine.
* [conrod](https://github.com/PistonDevelopers/conrod/) - An easy-to-use, immediate-mode, 2D GUI library written entirely in Rust.
* [dioxus](https://github.com/dioxuslabs/dioxus) - A portable, performant, and ergonomic framework for building cross-platform user interfaces in Rust.
* [Druid](https://github.com/linebender/druid) - Druid is an experimental Rust-native UI toolkit.
* [iced](https://github.com/iced-rs/iced) - A cross-platform GUI library for Rust focused on simplicity and type-safety. Inspired by Elm.
* [OrbTk](https://github.com/redox-os/orbtk) - The Orbital Widget Toolkit is a cross-platform (G)UI toolkit for building scalable user interfaces with the programming language Rust. It's based on the [Entity Component System Pattern](https://en.wikipedia.org/wiki/Entity_component_system) and provides a functional [Reactive-like API](https://en.wikipedia.org/wiki/Functional_reactive_programming).
* [Pake](https://github.com/tw93/Pake) - Turn any webpage into a desktop app with Rust with ease.
* [rise-ui](https://github.com/rise-ui/rise) - Simple component-based cross-Platform GUI Toolkit for developing beautiful and user-friendly interfaces.
* [rust_minifb](https://github.com/emoon/rust_minifb) - minifb is a cross-platform window setup with optional bitmap rendering. It also comes with easy mouse and keyboard input. Primarily designed for prototyping.
* [xilem](https://github.com/linebender/xilem) — Successor of the data-first Rust-native UI design toolkit [druid](https://github.com/linebender/druid).

* Cocoa
  * [sorbet-cocoa](https://github.com/kylewlacy/sorbet-cocoa)
  * [core-foundation-rs](https://github.com/servo/core-foundation-rs)
* [Flutter](https://flutter.dev/)
  * [flutter-rs](https://github.com/gliheng/flutter-rs) - Build flutter desktop app in dart & rust.
  * [flutter_rust_bridge](https://github.com/fzyzcjy/flutter_rust_bridge) - High-level memory-safe binding generator for Flutter/Dart <-> Rust.
  * [rinf](https://github.com/cunarist/rinf) - Rust as your Flutter backend, Flutter as your Rust frontend.
* [GTK+](https://www.gtk.org/) [[gtk](https://crates.io/keywords/gtk)]
  * [gtk](https://github.com/gtk-rs/gtk) - GTK+ bindings.
  * [relm](https://github.com/antoyo/relm) - Asynchronous, GTK+-based, GUI library, inspired by Elm.
* [ImGui](https://github.com/ocornut/imgui)
  * [imgui-rs](https://github.com/Gekkio/imgui-rs) - Rust bindings for ImGui.
* [IUP](http://webserver2.tecgraf.puc-rio.br/iup/)
  * [clear-coat](https://github.com/jminer/clear-coat) - Clear Coat is a Rust wrapper for the IUP GUI library
  * [iup-rust](https://github.com/dcampbell24/iup-rust) - IUP bindings.
  * [Kiss-ui](https://github.com/KISS-UI/kiss-ui) - A simple UI framework built on IUP.
* [libui](https://github.com/andlabs/libui)
  * [libui-rs](https://github.com/pcwalton/libui-rs) - libui bindings. Abandoned project. The most updated fork is [NoraCodes/libui-rs](https://github.com/NoraCodes/libui-rs).
* [makepad](https://github.com/makepad/makepad) - Makepad is a creative software development platform for Rust that compiles to wasm/webGL, osx/metal, windows/dx11 linux/opengl.
* [Nuklear](https://github.com/vurtun/nuklear)
  * [nuklear-rust](https://github.com/snuk182/nuklear-rust) - Rust bindings for Nuklear.
* [Qt](https://doc.qt.io)
  * [qmetaobject-rs](https://github.com/woboq/qmetaobject-rs) - Integrate Qml and Rust by building the QMetaObject at compile time.
  * [qmlrs](https://github.com/cyndis/qmlrs) - QtQuick bindings.
  * [qt.rs](https://github.com/kitech/qt.rs) - Qt5 bindings.
  * [Rust Qt Binding Generator](https://phabricator.kde.org/source/rust-qt-binding-generator/) - Binding generator hosted by KDE.
  * [rust-qt](https://github.com/rust-qt) -
  * [qml-rust](https://github.com/White-Oak/qml-rust) - QML bindings.
* [nfd-rs](https://github.com/saurvs/nfd-rs) - [nativefiledialog](https://github.com/mlabbe/nativefiledialog) bindings
* [Sciter](https://sciter.com/)
  * [rust-sciter](https://github.com/sciter-sdk/rust-sciter) - Sciter bindings.

### Image processing

* [cv](https://github.com/rust-cv/cv) - Rust CV is a project to implement computer vision algorithms, abstractions, and systems in Rust. #[no_std] is supported where possible.
* [dominant_color](https://github.com/marekm4/dominant_color) - Dominant color extractor.
* [img_hash](https://github.com/abonander/img_hash) - Perceptual image hashing and comparison for equality and similarity.
* [image](https://github.com/image-rs/image) - Basic imaging processing functions and methods for converting to and from image formats.
* [imageproc](https://github.com/image-rs/imageproc) - An image processing library, based on the `image` library.
* [opencv-rust](https://github.com/twistedfall/opencv-rust) - Rust bindings for OpenCV.
* [oxipng](https://github.com/shssoichiro/oxipng) - Multithreaded PNG optimizer written in Rust.
* [steganography](https://github.com/teovoinea/steganography) [[steganography](https://crates.io/crates/steganography)] - A simple steganography library.

### Language specification

* [bnf](https://github.com/shnewto/bnf) - A library for parsing Backus–Naur form context-free grammars.

### Logging

[[log](https://crates.io/keywords/log)]

* [fast_log](https://github.com/rbatis/fast_log) - Rust async log High-performance asynchronous logging.
* [log](https://github.com/rust-lang-nursery/log) - Logging implementation for Rust.
* [log](https://github.com/rust-lang/log) - Logging implementation for Rust.
* [slog](https://github.com/slog-rs/slog) - Structured, composable logging for Rust.
* [log4rs](https://github.com/sfackler/log4rs) - highly configurable logging framework modeled after Java's Logback and log4j libraries.
* [paris](https://github.com/0x20F/paris) - Simple way to output beautiful text in your CLI applications. Only limit is your imagination.
* [tailspin](https://github.com/bensadeh/tailspin) - A log file highlighter.
* [tracing](https://github.com/tokio-rs/tracing) - An application level tracing framework for async-aware structured logging, error handling, metrics, and more.

### Macro

* cute
  * [cute](https://github.com/mattgathu/cute) - Macro for Python-esque list comprehensions in Rust.
* hado
  * [hado-rs](https://github.com/ludat/hado-rs) - A little macro for writing haskell-like do expressions without too much ceremony

### Markup language

* CommonMark
  * [pulldown-cmark](https://github.com/pulldown-cmark/pulldown-cmark) - [CommonMark](https://commonmark.org/) parser in Rust.
  * [tidier](https://github.com/insomnimus/tidier) - A library to format HTML, XHTML and XML documents.

### Memory

* [memmap-rs](https://github.com/danburkert/memmap-rs) - cross-platform Rust API for memory mapped IO.
* [rpmalloc-rs](https://github.com/EmbarkStudios/rpmalloc-rs) - Cross-platform Rust global memory allocator using [rpmalloc](https://github.com/rampantpixels/rpmalloc).

### Messaging

* [floki](https://github.com/arthurprs/floki) - Floki Message Queue.

## Miscellaneous

### Uncategorized

* [lance](https://github.com/eto-ai/lance) - Lance is a columnar data format that is easy and fast to version, query and train on. It’s designed to be used with images, videos, 3D point clouds, audio and of course tabular data. Alternative to Parquet. 100x faster for random access, includes a vector index and automatic versioning. Apache Arrow and DuckDB compatible.

### Mobile

* Generic
  * [rust_on_mobile](https://github.com/Geal/rust_on_mobile)
  * [redbadger/crux](https://github.com/redbadger/crux) - Cross-platform app development in Rust. Crux helps you share your app's business logic and behavior across mobile (iOS/Android) and web — as a single reusable core built with Rust.

* Android
  * [android-rs-glue](https://github.com/rust-windowing/android-rs-glue) - glue between Rust and Android.
* iOS
  * [cargo-lipo](https://github.com/TimNN/cargo-lipo) - A cargo lipo subcommand which automatically creates a universal library for use with your iOS application.
  * [ObjCrust](https://github.com/vhbit/ObjCrust) - using Rust to create an iOS static library.
* Pebble
  * [pebble.rs](https://github.com/andars/pebble.rs) - A crate that allows Rust to be used to develop Pebble applications.

### Network programming

* HTTP
  * [Hurl](https://github.com/Orange-OpenSource/hurl) — Run and test HTTP requests with plain text and libcurl.
  * [parallel-getter](https://github.com/pop-os/parallel-getter) - Download a file with parallel GET requests to maximize bandwidth usage.
  * [url-crawler](https://github.com/pop-os/url-crawler) - A configurable parallel web crawler, designed to crawl a website for content.
  * [url-scraper](https://github.com/pop-os/url-scraper) - Scrape URLs from HTML pages
* FTP
  * [rust-ftp](https://github.com/mattnenterprise/rust-ftp) - an [FTP](https://en.wikipedia.org/wiki/File_Transfer_Protocol) client for Rust.
* IPNetwork
  * [ipnetwork](https://github.com/achanda/ipnetwork) - A library to work with IP networks in pure Rust.
  * [netsim](https://github.com/canndrew/netsim) - A Rust library for network simulation and testing.
  * [online](https://github.com/jesusprubio/online) — Library to check your Internet connectivity.
* gRPC
  * [grpc-rs](https://github.com/tikv/grpc-rs) - The gRPC library for Rust built on C Core library and futures.
  * [Tonic](https://github.com/hyperium/tonic) - A rust implementation of gRPC, a high performance, open source, general RPC framework that puts mobile and HTTP/2 first.
* JSON-RPC
  * [futures-jsonrpc](https://github.com/vlopes11/futures-jsonrpc) [[futures-jsonrpc](https://crates.io/crates/futures-jsonrpc)] - Futures implementation for JSON-RPC.
* Low level
  * [actix](https://github.com/actix/actix) - Actor library for Rust.
  * [kay](https://github.com/aeplay/kay) - Experimental high-performance actor system framework for Rust.
  * [libpnet](https://github.com/libpnet/libpnet) - A cross-platform, low level networking.
  * [monoio](https://github.com/bytedance/monoio) - A thread-per-core Rust runtime with io_uring.
  * [pingora](https://github.com/cloudflare/pingora) - A library for building fast, reliable and evolvable network services.
  * [protocol](https://github.com/dylanmckay/protocol) - Custom TCP/UDP protocol definitions.  
  * [riker](https://github.com/riker-rs/riker) - Easily build efficient, highly concurrent and resilient applications. An Actor Framework for Rust.
  * [sendmmsg](https://github.com/Gymmasssorla/sendmmsg) - Transmit multiple messages using one system call in Rust.  
  * [tokio](https://github.com/tokio-rs/tokio) - A network application framework for rapid development and highly scalable production deployments of clients and servers.
  * [tower](https://github.com/tower-rs/tower) - Tower is a library of modular and reusable components for building robust networking clients and servers.
* MQTT
  * [rumqtt](https://github.com/bytebeamio/rumqtt) - A library for developers to build applications that communicate with the [MQTT protocol](https://mqtt.org/) over TCP and WebSockets, with or without TLS.
* NanoMsg
  * [nanomsg.rs](https://github.com/thehydroimpulse/nanomsg.rs) - [nanomsg](https://nanomsg.org/) bindings.
* Nng
  * [nng-rs](https://gitlab.com/neachdainn/nng-rs) [[Nng](http://crates.io/crates/nng)] - [Nng (nanomsg v2)](https://nanomsg.github.io/nng/index.html) bindings.
* NNTP
  * [rust-nntp](https://github.com/mattnenterprise/rust-nntp) - an [NNTP](https://en.wikipedia.org/wiki/Network_News_Transfer_Protocol) client for Rust.
* POP3
  * [rust-pop3](https://github.com/mattnenterprise/rust-pop3) - A [POP3](https://en.wikipedia.org/wiki/Post_Office_Protocol) client for Rust.
* Raknet
  * [rust-raknet](https://github.com/b23r0/rust-raknet) - RakNet Protocol implementation by Rust.
* Socket.io
  * [socketio](https://github.com/1c3t3a/rust-socketio) - an implementation of a [socket.io](https://socket.io/) client written in Rust.
* SSH
  * [ssh2-rs](https://github.com/alexcrichton/ssh2-rs) - [libssh2](https://www.libssh2.org/) bindings.
  * [Thrussh](https://github.com/pijul-scm/thrussh/) - an SSH library written from scratch in Rust, backed by [libsodium](https://download.libsodium.org/doc/)
* Stomp
  * [stomp-rs](https://github.com/zslayton/stomp-rs) - A [STOMP 1.2](http://stomp.github.io/stomp-specification-1.2.html) client implementation in Rust.
  * [tokio-stomp](https://github.com/adwhit/tokio-stomp) - An async [STOMP v1.1](https://stomp.github.io/stomp-specification-1.1.html) client for Rust, using the Tokio stack.
* uTP
  * [rust-utp](https://github.com/meqif/rust-utp) - A [uTP](http://www.bittorrent.org/beps/bep_0029.html) (Micro Transport Protocol) library for Rust.
* VPN
  * [wireguard-rs](https://github.com/DefGuard/wireguard-rs) - A multi-platform library providing a unified high-level API for managing WireGuard interfaces using native OS kernel and userspace WireGuard protocol implementations.  
* ZeroMQ
  * [rust-zmq](https://github.com/erickt/rust-zmq) - [ZeroMQ](http://zeromq.org/) bindings.
* CoAP
  * [coap-rs](https://github.com/Covertness/coap-rs) - A [Constrained Application Protocol(CoAP)](https://tools.ietf.org/html/rfc7252) library for Rust.
* Docker
  * [bollard](https://github.com/fussybeaver/bollard) - Docker daemon API in Rust
* RPC
  * [rpcx-rs](https://github.com/smallnest/rpcx-rs) - A RPC library for Rust for developing microservices in easy and simple way.
* QUIC
  * [neqo](https://github.com/mozilla/neqo) - an Implementation of QUIC written in Rust.
  * [quiche](https://github.com/cloudflare/quiche) - cloudflare implementation of the QUIC transport protocol and HTTP/3.
  * [quinn](https://github.com/djc/quinn) - Futures-based QUIC implementation in Rust.
  * [s2n-quic](https://github.com/aws/s2n-quic) - An implementation of the IETF QUIC protocol.
* P2P
  * [rust-libp2p](https://github.com/libp2p/rust-libp2p) - The Rust Implementation of libp2p networking stack.
* NATS
  * [nats.rs](https://github.com/nats-io/nats.rs) - Rust client for NATS, the cloud native messaging system.

### Parsing

  * [chomp](https://github.com/m4rw3r/chomp) - A fast monadic-style parser combinator.
  * [combine](https://github.com/Marwes/combine) - parser combinator library.
  * [lady-deirdre](https://github.com/Eliah-Lakhin/lady-deirdre) - A framework for new programming languages and LSP servers.
  * [lalrpop](https://github.com/lalrpop/lalrpop) - LR(1) parser generator for Rust.
  * [libazdice](https://github.com/aleshaleksey/libazdice) - A parser for parsing dice strings for TTRPGs and TTRPG development
  * [nom](https://github.com/Geal/nom) - parser combinator library.
  * [oak](https://github.com/ptal/oak) - A typed PEG parser generator (compiler plugin)
  * [inquerest](https://github.com/ivanceras/inquerest) - an URL parameter parser for rest filter inquiry.
  * [rust-peg](https://github.com/kevinmehall/rust-peg) - Parsing Expression Grammar (PEG) parser generator.
  * [rust-shlex](https://github.com/comex/rust-shlex) - Split a string into shell words, like Python's shlex.
  * [robotstxt](https://github.com/Folyd/robotstxt) - A native Rust port of Google's robots.txt parser and matcher C++ library.
  * [tree-sitter](https://github.com/tree-sitter/tree-sitter) - A parser generator tool and an incremental parsing library geared towards programming tools.
  * [pest](https://github.com/pest-parser/pest) - The Elegant Parser.
  * [queryst](https://github.com/s-panferov/queryst) - A query string parsing library for Rust inspired by https://github.com/ljharb/qs
  * [jsonpath](https://github.com/freestrings/jsonpath) - [JsonPath](https://goessner.net/articles/JsonPath/) engine written in Rust. Webassembly and Javascript support too.
  * [wavefront_rs](https://github.com/replicadse/wavefront_rs) - A parser for the Wavefront OBJ format.
  * [zero](https://github.com/nrc/zero) - zero-allocation parsing of binary data.

### Packaging formats

- [debarchive](https://github.com/pop-os/debarchive) Library for reading and extracting debian archives

### Peripherals

* Serial Port
  * [serialport-rs](https://github.com/serialport/serialport-rs) [[serialport](https://docs.rs/serialport/3.0.0/serialport/)] - A cross-platform library that provides access to a serial port

### Platform specific

* Linux
  * [cgroups-fs](https://github.com/frol/cgroups-fs) - Rust bindings to Linux Control Groups (cgroups).
  * [dbus-udisks2](https://github.com/pop-os/dbus-udisks2) - UDisks2 DBus API
  * [distinst](https://github.com/pop-os/distinst/) - Linux distribution installer library
  * [inotify](https://github.com/inotify-rs/inotify) - [inotify](https://en.wikipedia.org/wiki/Inotify) bindings.
  * [linux-futex](https://github.com/m-ou-se/linux-futex) - Linux-specific fast user-space locking primitive.
  * [nginx-rs](https://github.com/arvancloud/nginx-rs) - [Nginx](https://www.nginx.com) bindings.
  * [rust-iptables](https://github.com/yaa110/rust-iptables) - [iptables](https://www.netfilter.org/projects/iptables/index.html) bindings.
* Unix-like
  * [nix](https://github.com/nix-rust/nix) - Unix-like API bindings.
  * [rust-fuse](https://github.com/zargony/rust-fuse) - [FUSE](https://github.com/libfuse/libfuse) bindings.
* Windows
  * [winapi-rs](https://github.com/retep998/winapi-rs) - Windows API bindings.
  * [microsoft/windows-rs](https://github.com/microsoft/windows-rs) — Rust for Windows.
* FreeBSD
  * [libjail-rs](https://github.com/fubarnetes/libjail-rs/) - Rust implementation of a FreeBSD jail library.
  * [capsicum-rs](https://github.com/dlrobertson/capsicum-rs) - Rust bindings for the FreeBSD capsicum framework.

### QrCode

*Libraries for manipulating with QrCodes.*

* [qrcode-rust](https://github.com/kennytm/qrcode-rust) - QR code encoder in Rust.


### Scripting

[[scripting](https://crates.io/keywords/scripting)]

* [duckscript](https://crates.io/crates/duckscript) -  Simple, extendable and embeddable scripting language.
* [dyon](https://github.com/PistonDevelopers/dyon) - A rusty dynamically typed scripting language.
* [gluon](https://github.com/gluon-lang/gluon) -  A small, statically-typed, functional programming language
* [KCLVM](https://github.com/KusionStack/KCLVM) - A constraint-based record & functional language mainly used in configuration and policy scenarios.
* [ketos](https://github.com/murarth/ketos) - A Lisp dialect functional programming language serving as a scripting and extension language for rust
* [moss](https://crates.io/crates/moss) - A dynamically typed scripting language
* [rhai](https://github.com/jonathandturner/rhai) - A tiny and fast embedded scripting language resembling a combination of JS and Rust

### Simulation

* [bigbang](https://crates.io/crates/bigbang) - Gravitational and collisional n-body simulation with optional GPU acceleration.
* [hEngine](https://github.com/hashintel/hash/tree/main/packages/engine) - A Rust-implemented computational simulation engine, supporting large-scale agent-based modelling, with simulation logic written in JavaScript and Python.
* [nyx-space](https://crates.io/crates/nyx-space) - High fidelity, fast, reliable and validated astrodynamical toolkit library, used for spacecraft mission design and orbit determination.

### Task scheduling

* [delay-timer](https://github.com/BinChengZhao/delay-timer) - delay-timer is a task manager based on a time wheel algorithm, which makes it easy to manage timed tasks, or to periodically execute arbitrary tasks such as closures.
* [delicate](https://github.com/BinChengZhao/delicate) - A lightweight and distributed task scheduling platform written in rust.
* [tasklet](https://github.com/stav121/tasklet) - A task scheduling library written in Rust.

### Template engine

* Handlebars
  * [handlebars-rust](https://github.com/sunng87/handlebars-rust) - Handlebars template engine with inheritance, custom helper support.
  * [yarte](https://gitlab.com/r-iendo/yarte) - compile-time handlebars-like templates
* HTML
  * [maud](https://github.com/lfairy/maud) - compile-time HTML templates
  * [horrorshow-rs](https://github.com/Stebalien/horrorshow-rs) - compile-time HTML templates
  * [ructe](https://github.com/kaj/ructe) - HTML template system for Rust
  * [tera](https://github.com/Keats/tera) - template engine based on Jinja2 and the Django template language.
  * [askama](https://github.com/djc/askama) - template rendering engine based on Jinja.
  * [hiccup](https://github.com/naomijub/hiccup) - template engine inpired by Clojure's Hiccup.
* Mustache
  * [rustache](https://github.com/rustache/rustache) - Rust implementation of the Mustache specification.
* [marafet](https://github.com/tailhook/marafet) - Compiler for Jade-like template language to cito.js-based virtual dom
* [sailfish](https://github.com/Kogia-sima/sailfish) - Simple, small, and extremely fast template engine.

### Text processing

* [complate](https://github.com/replicadse/complate) - A in-terminal text templating tool designed for standardizing messages (like for GIT commits).
* [decancer](https://github.com/null8626/decancer) [[decancer](https://crates.io/crates/decancer)] — A tiny package that removes common unicode confusables/homoglyphs from strings.
* [easy_reader](https://github.com/ps1dr3x/easy_reader) - A reader that allows forwards, backwards and random navigations through the lines of huge files without consuming iterators.
* [ngrams](https://github.com/pwoolcoc/ngrams) - Construct [n-grams](https://en.wikipedia.org/wiki/N-gram) from arbitrary iterators.
* [suffix](https://github.com/BurntSushi/suffix) - Linear time suffix array construction (with Unicode support).
* [stringzilla](https://github.com/ashvardanian/StringZilla) - SIMD-accelerated string search, sort, edit distances, alignments, and generators for x86 AVX2 & AVX-512, and Arm NEON.
* [strsim-rs](https://crates.io/crates/strsim) - String similarity metrics.
* [tabwriter](https://github.com/BurntSushi/tabwriter) - Elastic tab stops (i.e., text column alignment).
* [textwrap](https://github.com/mgeisler/textwrap) - Word wrap text (with support for hyphenation).
* [regex](https://github.com/rust-lang/regex) - Regular expressions (RE2 style).
* [whatlang-rs](https://github.com/greyblake/whatlang-rs) - Natural language detection library based on trigrams.
* [rake-rs](https://github.com/yaa110/rake-rs) - Multilingual implementation of RAKE algorithm for Rust.
* [joinery](https://github.com/Lucretiel/joinery) – Generic string + iterable joining.

### Text search

* [MeiliSearch](https://github.com/meilisearch/MeiliSearch) - Is a powerful, fast, open-source, easy to use and deploy search engine. Both searching and indexing are highly customizable. Features such as typo-tolerance, filters, and synonyms are provided out-of-the-box.
* [fst](https://github.com/BurntSushi/fst) - Represent large sets and maps compactly with finite state transducers.
* [Perlin](https://github.com/CurrySoftware/perlin) - An Efficient and Ergonomic Document Search-Engine
* [pg_bm25](https://github.com/paradedb/paradedb/tree/dev/pg_bm25) - PostgreSQL extension that enables full text search over SQL tables using the BM25 algorithm, the state-of-the-art ranking function for full-text search.
* [tantivy](https://github.com/quickwit-oss/tantivy) - full text search engine library written in Rust.

### Video

* [ffmpeg-sidecar](https://github.com/nathanbabcock/ffmpeg-sidecar) - Wrap a standalone FFmpeg binary in an intuitive Iterator interface.

### Virtualization

* [quantum](https://github.com/beneills/quantum) - Advanced Rust quantum computer simulator.
* [crosvm](https://chromium.googlesource.com/chromiumos/platform/crosvm/) CrOSVM - Enables Chrome OS to run Linux apps inside a fast, sercure virtualized environment
* [unicorn-rs](https://github.com/ekse/unicorn-rs) - Rust bindings for the unicorn CPU emulator
* [hypervisor-rs](https://github.com/saurvs/hypervisor-rs) - Hardware-accelerated virtualization on OS X

### Web programming

See also [Are we web yet?](http://www.arewewebyet.org) and [Rust web framework comparison](https://github.com/flosse/rust-web-framework-comparison).

* Client-side / WASM
  * [cargo-web](https://crates.io/crates/cargo-web) - A Cargo subcommand for the client-side Web.
  * [stdweb](https://crates.io/crates/stdweb) - A standard library for the client-side Web.
  * [yew](https://crates.io/crates/yew) - Rust framework for making client web apps
* Gemini
  * [agate](https://github.com/mbrubeck/agate) - Very simple server for the [Gemini](https://gemini.circumlunar.space/) hypertext protocol.
* HTTP Client
  * [async-graphql](https://github.com/async-graphql/async-graphql) - A GraphQL server library implemented in Rust.
  * [curl-rust](https://github.com/alexcrichton/curl-rust) - [libcurl](https://curl.haxx.se/libcurl/) bindings.
  * [hyper](https://github.com/hyperium/hyper) - relatively low-level library, meant to be a building block for libraries and applications.
  * [reqwest](https://github.com/seanmonstar/reqwest) - an ergonomic HTTP Client for Rust.
  * [yukikaze](https://gitlab.com/Douman/yukikaze) - Beautiful and elegant Yukikaze is little HTTP client library based on hyper.
  * [graphql-client](https://github.com/graphql-rust/graphql-client) — Typed, correct GraphQL requests and responses in Rust.
* HTTP Server
  * [actix-web](https://github.com/actix/actix-web) - A lightweight async web framework for Rust with websocket support.
  * [Anansi](https://github.com/saru-tora/anansi) - A simple full-stack web framework for Rust.
  * [axum](https://github.com/tokio-rs/axum) - web application framework that focuses on ergonomics and modularity.
  * [branca](https://crates.io/crates/branca) - A Pure Rust implementation of Branca for Authenticated and Encrypted API tokens.
  * [Gotham](https://github.com/gotham-rs/gotham) - A flexible web framework that does not sacrifice safety, security or speed.
  * [hyper](https://github.com/hyperium/hyper) - an HTTP implementation.
  * [frank_jwt](https://github.com/GildedHonour/frank_jwt) - JSON Web Token implementation in Rust.
  * [juniper](https://github.com/graphql-rust/juniper) - Juniper makes it possible to write GraphQL servers in Rust that are type-safe and blazingly fast. We also try to make declaring and resolving GraphQL schemas as convenient as Rust will allow.
  * [handlebars-rust](https://github.com/sunng87/handlebars-rust) - an Iron web framework middleware.
  * [Iron](https://github.com/iron/iron) - A middleware-based server framework.
  * [Nickel](https://github.com/nickel-org/nickel.rs/) - inspired by [Express](http://expressjs.com/)
  * [poem](https://github.com/poem-web/poem) - A full-featured and easy-to-use web framework with the Rust programming language.
  * [oxidy](https://github.com/oxidy-rs/oxidy) - Super Fast & High Performance minimalist web framework for rust.
  * [Resty](https://github.com/tomusdrw/resty) - simple JSON REST-API framework for Rust.
  * [rustful](https://github.com/Ogeon/rustful) - A RESTful web framework for Rust.
  * [Rocket](https://github.com/SergioBenitez/Rocket) - Rocket is web framework for Rust (nightly) with a focus on ease-of-use, expressability, and speed.
  * [Rustless](https://github.com/rustless/rustless) - A REST-like API micro-framework inspired by [Grape](https://github.com/ruby-grape/grape) and [Hyper](https://github.com/hyperium/hyper)
  * [Saphir](https://github.com/richerarc/saphir) - A progressive web framework with low-level control, without the pain.
  * [sapper](https://github.com/rustforce/sapper) - A lightweight web framework built on async hyper, implemented in Rust language.
  * [tiny-http](https://github.com/tiny-http/tiny-http) - Low level HTTP server library.
  * [rouille](https://github.com/tomaka/rouille) - Web framework in Rust.
  * [tower-web](https://github.com/carllerche/tower-web) - A fast, boilerplate free, web framework for Rust.
  * [sincere](https://github.com/danclive/sincere) - A micro web framework for Rust(stable) based on hyper and multithreading.
  * [zap](https://github.com/oltdaniel/zap) - A lightning fast http framework for Rust.
  * [xitca-web](https://github.com/HFQR/xitca-web) - An alternative http library and web framework inspired by hyper and actix-web.
  * [Zino](https://github.com/zino-rs/zino) - Next-generation framework for composable applications.
* [WebSocket](https://datatracker.ietf.org/doc/rfc6455/)
  * [fastwebsockets](https://github.com/denoland/fastwebsockets) - A fast RFC6455 WebSocket implementation.
  * [sockjs](https://github.com/actix/sockjs) - A [SockJS](https://github.com/sockjs) server for Rust.
  * [rust-websocket](https://github.com/cyderize/rust-websocket) - A framework for dealing with WebSocket connections (both clients and servers).
  * [ws-rs](https://github.com/housleyjk/ws-rs) - lightweight, event-driven WebSockets for Rust.
  * [tungstenite-rs](https://github.com/snapview/tungstenite-rs) - Lightweight stream-based WebSocket implementation for Rust.
  * [websocat](https://github.com/vi/websocat) - CLI for interacting with WebSockets, with functionality of Netcat, Curl and Socat.
  * [urlshortener-rs](https://github.com/vityafx/urlshortener-rs) - A very simple urlshortener library for Rust.
* Miscellaneous
  * [cargonauts](https://github.com/cargonauts-rs/cargonauts) - A web framework intended for building maintainable, well-factored web apps.
  * [oso](https://github.com/osohq/oso) - A policy engine for authorization that's embedded in your application.
  * [rust-embed](https://github.com/pyros2097/rust-embed) - A macro to embed static assets into the rust binary.
  * [serenity](https://github.com/serenity-rs/serenity) - A Rust library for the Discord API.
  * [openapi](https://github.com/softprops/openapi) - A library for processing openapi spec files.
  * [rookie](https://github.com/thewh1teagle/rookie) - Load cookies from any browser on any platform.
  * [svix-webhooks](https://github.com/svix/svix-webhooks) - A library for sending webhooks and verifying signatures.
  * [tbot](https://gitlab.com/SnejUgal/tbot) - Make cool Telegram bots with Rust easily.
  * [teloxide](https://github.com/teloxide/teloxide/) - An elegant Telegram bots framework for Rust.
  * [select.rs](https://github.com/utkarshkukreti/select.rs)- A library to extract useful data from HTML documents, suitable for web scraping.
  * [soup](https://gitlab.com/pwoolcoc/soup) - A library similar to Pythons BeautifulSoup, designed to enable quick and easy manipulation and querying of HTML documents.
  * [valitron](https://github.com/tu6ge/valitron) - An ergonomic, functional and configurable validator.
  * [xh](https://github.com/ducaale/xh) - Friendly and fast tool for sending HTTP requests.
* Reverse Proxy
  * [sozu](https://github.com/sozu-proxy/sozu) - A HTTP reverse proxy.
* Static Site Generators
  * [zola](https://github.com/getzola/zola) [[zola](https://www.getzola.org/)] - An opinionated static site generator with everything built-in.
  * [cobalt.rs](https://github.com/cobalt-org/cobalt.rs) - Static site generator written in Rust.
  * [mdblog.rs](https://github.com/FuGangqiang/mdblog.rs) - Static site generator from markdown files.
  * [leven](https://github.com/leven-the-blog/leven) - A simple, parallelized blog generator.  

## Registries

A registry allows you to publish your Rust libraries as crate packages, to share them with others publicly and privately.

* [chartered](https://github.com/w4/chartered) - A private, authenticated, permissioned Cargo registry.
* [Crates](https://crates.io) - The official public registry for Rust/Cargo.
* [Cloudsmith](https://cloudsmith.com/product/formats/cargo-registry) - A fully managed package management SaaS, with first-class support for public and private Cargo/Rust registries (plus many others). Has a generous free-tier and is also completely free for open-source.

## Resources

* Benchmarks
  * [benchmarksgame-rs](https://github.com/TeXitoi/benchmarksgame-rs) - Rust implementations for the [The Computer Language Benchmarks Game](https://benchmarksgame-team.pages.debian.net/benchmarksgame/).
* Books
  * [async-book](https://github.com/rust-lang/async-book) - Asynchronous Programming in Rust.
  * [RustBook](https://github.com/QMHTMY/RustBook) - A book about Rust Data Structures and Algorithms.
* Decks & Presentations
  * [Learning systems programming with Rust](https://speakerdeck.com/jvns/learning-systems-programming-with-rust) - Presented by [Julia Evans](https://twitter.com/@b0rk) @ Rustconf 2016.
  * [Shipping a Solid Rust Crate](https://www.youtube.com/watch?v=t4CyEKb-ywA) - Presented by [Michael Gattozzi](https://github.com/mgattozzi) @ RustConf 2017
  * [Rust: Hack Without Fear!](https://www.youtube.com/watch?v=lO1z-7cuRYI) - Presented by [Nicholas Matsakis](https://github.com/nikomatsakis) @ C++Now 2018
* Learning
  * [Aquascope](https://github.com/cognitive-engineering-lab/aquascope) - Interactive visualizations of Rust at compile-time and run-time.
  * [Awesome Rust Streaming](https://github.com/jamesmunns/awesome-rust-streaming) - A community curated list of livestreams about Rust.
  * [awesome-rust-mentors](https://rustbeginners.github.io/awesome-rust-mentors/) - A list of helpful Rust mentors willing to take mentees and educate them about Rust and programming.
  * [Concurrency programming via rust](https://github.com/smallnest/concurrency-programming-via-rust) - conncurrency programming via rust.
  * [How to learn modern Rust](https://github.com/joaocarvalhoopen/How_to_learn_modern_Rust) - A guide to the adventurer.
  * [Rust Gym](https://github.com/warycat/rustgym) - A big collection of coding interview problems solved in Rust.
  * [Programming Community Curated Resources for Learning Rust](https://hackr.io/tutorials/learn-rust) - A list of recommended resources voted by the programming community.
  * [exercism.io](https://exercism.io/tracks/rust) - programming exercises that help you learn new concepts in Rust.
  * [Idiomatic Rust](https://github.com/mre/idiomatic-rust) -  A peer-reviewed collection of articles/talks/repos which teach idiomatic Rust.
  * [Learn Rust by 500 lines code](https://github.com/cuppar/rtd) - Learn Rust by 500 lines code, build a Todo Cli Application from scratch.
  * [Learning Rust With Entirely Too Many Linked Lists](http://cglab.ca/~abeinges/blah/too-many-lists/book/) - in-depth exploration of Rust's memory management rules, through implementing a few different types of list structures.
  * [Rust by Example](https://doc.rust-lang.org/rust-by-example/)
  * [Rust Cookbook](https://rust-lang-nursery.github.io/rust-cookbook/) - A collection of simple examples that demonstrate good practices to accomplish common programming tasks, using the crates of the Rust ecosystem.
  * [Rust Flashcards](https://github.com/ad-si/Rust-Flashcards) - Over 550 flashcards to learn Rust from first principles.
  * [Rust Online Courses at Classpert](https://classpert.com/it-computer-science/software-development/rust-programming) - A list of Rust online courses (paid) from Classpert Online Course Search.
  * [Rust for professionals](https://overexact.com/rust-for-professionals/) - A quick introduction to Rust for experienced software developers.
  * [Rust in Motion](https://www.manning.com/livevideo/rust-in-motion?a_aid=cnichols&a_bid=6a993c2e) - A video series by [Carol Nichols](https://github.com/carols10cents) and [Jake Goulding](https://github.com/shepmaster) (paid)
  * [rust-learning](https://github.com/ctjhoa/rust-learning) - A collection of useful resources to learn Rust
  * [Rustlings](https://github.com/rust-lang/rustlings) - small exercises to get you used to reading and writing Rust code
  * [stdx](https://github.com/brson/stdx) - Learn these crates first as an extension to std.
  * [Take your first steps with Rust](https://learn.microsoft.com/en-us/training/paths/rust-first-steps/) - Lay the foundation of knowledge you need to build fast and effective programs in Rust.
  * [University of Pennsylvania's Comp Sci Rust Programming Course](http://cis198-2016s.github.io/schedule/)
  * [Build a language VM](https://blog.subnetzero.io/post/building-language-vm-part-00/)
  * [Rust Web Development Tutorial](https://cloudmaker.dev/how-to-create-a-rest-api-in-rust/) - Rust Web Development Tutorial: REST API.
  * [Rust Web Developer Roadmap](https://github.com/QMHTMY/rust-web-developer-roadmap) - Roadmap to becoming a Rust Web Developer in 2021.
* Podcasts
  * [New Rustacean](https://newrustacean.com) - A podcast about learning Rust
  * [Rusty Spike](https://rusty-spike.blubrry.net) - news on all things Rust
  * [rust-blog](https://github.com/pretzelhammer/rust-blog) - educational content for Rust beginners and Rust advanced beginners.
* [Discover Rust Libraries & Code Snippets](https://kandi.openweaver.com/explore/rust) - A curated list of Rust libraries, authors, kits, tutorials & learning resources on kandi.  
* [RustCamp 2015 Talks](http://confreaks.tv/events/rustcamp2015)
* [Rust Design Patterns](https://github.com/rust-unofficial/patterns)
* [Rust Guidelines](http://aturon.github.io/)
* [RustBooks](https://github.com/sger/RustBooks) - list of RustBooks
* [Rust Subreddit](https://www.reddit.com/r/rust/) - A subreddit(forum) where rust related questions, articles and resources are posted and discussed
